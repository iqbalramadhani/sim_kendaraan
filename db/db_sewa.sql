-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2020 at 11:49 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sewa`
--

-- --------------------------------------------------------

--
-- Table structure for table `ban`
--

CREATE TABLE `ban` (
  `id_ban` char(5) NOT NULL,
  `nomor` varchar(20) NOT NULL,
  `harga` double NOT NULL,
  `merk` varchar(20) NOT NULL,
  `harga_claim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ban`
--

INSERT INTO `ban` (`id_ban`, `nomor`, `harga`, `merk`, `harga_claim`) VALUES
('B0001', 'D6L3 K4267', 375000, 'BRIGESTONE', 107),
('B0002', 'D7A2 K4317', 3750000, 'BRIGESTONE', 107),
('B0003', '5723 9070 518', 3450000, 'MRF MUSCLE MILLER', 86),
('B0004', '5723 9890 518', 3450000, 'MRF MUSCLE MILLER', 86),
('B0005', 'D721 120 829', 3750000, 'WEST LAKE CM 913', 107),
('B0006', 'D721 101 917', 3750000, 'WEST LAKE CM 913', 107),
('B0007', 'A821 061 341', 3450000, 'GOODRIDE CM 998', 99),
('B0008', 'A821 175 158', 3450000, 'GOODRIDE CM 998', 99),
('B0009', 'A821 264 141', 3450000, 'GOODRIDE CM 998', 99),
('B0010', '7016 8740 218', 4100000, 'MRF S3C8 IMS', 103),
('B0011', '7022 813 0218', 4300000, 'MRF S3C8', 108),
('B0012', '7024 072 0218', 4300000, 'MRF S3C8', 108),
('B0013', '7022 421 0218', 4300000, 'MRF S3C8', 108),
('B0014', '7022 694 0218', 4300000, 'MRF S3C8', 108),
('B0015', 'T4018 7275', 3375000, 'GT MILLER', 135),
('B0016', 'T4018 6931', 3375000, 'GT MILLER', 135),
('B0017', 'C1107 748 1118', 4330000, 'KAWAT APOLLO', 108),
('B0018', 'C1100 289 1118', 4330000, 'KAWAT APOLLO', 108),
('B0019', 'D721 204 799', 3800000, 'WEST LAKE CM 913', 109),
('B0020', 'B721 134 939', 3800000, 'WEST LAKE CM 913', 109);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `kode_booking` int(8) NOT NULL,
  `id_periode` char(4) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tgl_booking` date NOT NULL,
  `tujuan` varchar(200) NOT NULL,
  `jemput` varchar(200) NOT NULL,
  `jam` time NOT NULL,
  `tgl_berangkat` date NOT NULL,
  `tgl_pulang` date NOT NULL,
  `id_kendaraan` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`kode_booking`, `id_periode`, `nama`, `tgl_booking`, `tujuan`, `jemput`, `jam`, `tgl_berangkat`, `tgl_pulang`, `id_kendaraan`) VALUES
(1, '719', 'Bp. Rohim', '0000-00-00', ' Bandara, L. Batang, M Priuk & Tugu Monas', 'Ciomas', '00:00:00', '2019-07-29', '2019-07-29', 'Bus7508'),
(2, '719', 'Bp. Rohim', '0000-00-00', ' Bandara, L. Batang, M Priuk & Tugu Monas', 'Ciomas', '00:00:00', '2019-07-29', '2019-07-29', 'Bus7507'),
(3, '819', 'Bp. Arion', '0000-00-00', 'TBN Cipanas, TWM, Tajur', '', '00:00:00', '2019-08-24', '2019-08-25', 'Bus7507'),
(4, '819', 'Bp. Arion', '0000-00-00', 'TBN Cipanas, TWM, Tajur', '', '00:00:00', '2019-08-24', '2019-08-25', 'Bus7507'),
(5, '819', 'Bp. Imron ', '0000-00-00', 'Puncak Bogor', 'Bandung ', '00:00:00', '2019-08-14', '2019-08-14', 'Bus7508'),
(6, '819', 'Ibu Indah', '0000-00-00', 'Ancol', 'Cilegon', '00:00:00', '2019-08-14', '2019-08-14', 'Bus7508'),
(7, '819', 'Bp. Munadzir', '2019-01-08', 'Ancol', 'Masjid Agung Cilegon s/d Ciwandan', '00:00:00', '2019-08-28', '2019-08-28', 'Bus7507'),
(8, '819', 'Bp. Munadzir', '2019-01-08', 'Ancol', 'Masjid Agung Cilegon s/d Ciwandan', '00:00:00', '2019-08-28', '2019-08-28', 'Bus7508'),
(9, '819', 'Bp. Salam', '2019-03-08', 'Bandung', 'Pasir Bolang Tiga Raksa', '00:00:00', '2019-08-05', '2019-08-05', 'Bus7551'),
(10, '819', 'Bp. Salam', '2019-03-08', 'Bandung', 'Pasir Bolang Tiga Raksa', '00:00:00', '2019-08-05', '2019-08-05', 'Bus7552'),
(11, '919', 'Bp. Ahmad', '2019-04-08', 'Cirebon', 'Bandung', '00:00:00', '2019-09-12', '2019-09-12', 'Bus7508'),
(12, '819', 'Bu. Heni', '2019-04-08', 'Seaworld & Gelanggang Samudra', 'Ketileng Barat', '00:00:00', '2019-08-20', '2019-08-20', 'Bus7551'),
(13, '819', 'Bu. Ririn', '2019-04-08', 'Water Kingdom Mekarsari', 'Masjid Agung', '00:00:00', '2019-08-17', '2019-08-17', 'Bus7552'),
(14, '919', 'Bu Intan', '2020-05-08', 'Ancol', 'Baluk Cikande', '00:00:00', '2019-09-02', '2019-09-02', 'Bus7553'),
(15, '919', 'Bp. Dindin', '2020-05-08', 'Taman Safari', 'KP Cina', '00:00:00', '2019-09-03', '2019-09-03', 'Bus7552'),
(16, '819', 'Bp. Imanudin', '2019-06-08', 'Farmhouse', 'Cilegon', '00:00:00', '2019-08-22', '2019-08-22', 'Bus7553'),
(17, '819', 'Bp. Imanudin', '2019-06-08', 'Farmhouse', 'Cilegon', '00:00:00', '2019-08-22', '2019-08-22', 'Bus7507'),
(18, '919', 'Bu Imania', '2019-06-08', 'Cikole', 'Tegal Wangi', '00:00:00', '2019-09-04', '2019-09-04', 'Bus7508'),
(19, '819', 'Bp. Helmi', '2019-07-08', 'Ancol', 'Kerenceng', '07:12:00', '2019-08-27', '2019-08-27', 'Bus7507'),
(20, '819', 'Bp. Helmi', '2019-07-08', 'Ancol', 'Kerenceng', '07:12:00', '2019-08-27', '2019-08-27', 'Bus7507'),
(21, '919', 'Bu. Eka', '2019-07-08', 'Jakarta', 'TKIT Cendikia', '07:12:00', '2019-09-06', '2019-09-06', 'Bus7508'),
(22, '919', 'Bu. Eka', '2019-07-08', 'Jakarta', 'TKIT Cendikia', '07:12:00', '2019-09-06', '2019-09-06', 'Bus7507'),
(23, '919', 'Bu. Yanti', '2019-10-08', 'Monas & Ragunan', 'Kntr Ds. Penggarengan Bojonegara', '00:00:00', '2019-09-10', '2019-09-10', 'Bus7551'),
(24, '919', 'Bu. Yanti', '2019-10-08', 'Monas & Ragunan', 'Kntr Ds. Penggarengan Bojonegara', '00:00:00', '2019-09-10', '2019-09-10', 'Bus7552'),
(25, '819', 'Bu Dina', '2019-11-08', 'Cikole', 'Kp Cina', '00:00:00', '2019-08-29', '2019-08-29', 'Bus7507'),
(26, '919', 'Pa Mahmud', '2019-11-08', 'Keong Mas', 'Bandung ', '00:00:00', '2019-09-06', '2019-09-06', 'Bus7508'),
(27, '919', 'Bu. Hj. Nadroh Jaohari', '2019-12-08', 'Jawa Tengah', 'STIA Cinanggung Serang', '00:00:00', '2019-09-16', '2019-09-16', 'Bus7508'),
(28, '919', 'Bu. Hj. Nadroh Jaohari', '2019-12-08', 'Jawa Tengah', 'STIA Cinanggung Serang', '00:00:00', '2019-09-15', '2019-09-17', 'Bus7507'),
(29, '819', 'Bp. Ozi ', '2019-12-08', 'Ancol', 'SDN Kuban Kalak Cilegon', '00:00:00', '2019-08-26', '2019-08-26', 'Bus7553'),
(30, '819', 'Bp. Ozi', '2019-12-08', 'Jungle Land', 'Giant Longtsr Serang', '00:00:00', '2019-08-21', '2019-08-21', 'Bus7552'),
(31, '919', 'Ponpes Nurul Abror', '2019-12-08', 'TWM & Puncak', 'Ponpes Nurul Abror', '07:12:00', '2019-09-02', '2019-09-02', 'Bus7552'),
(32, '819', 'Bp. Delvi', '0000-00-00', 'WOW Citra Raya Tanggerang', 'Merak', '00:00:00', '2019-08-23', '2019-08-23', 'Bus7553'),
(33, '819', 'Bp. Farid', '0000-00-00', 'Masjid Raya, Framhouse, Floating Market', 'CS Logistik Cilegon', '00:00:00', '2019-08-19', '2019-08-19', 'Bus7551'),
(34, '819', 'Bp. Farid', '0000-00-00', 'Masjid Raya, Framhouse, Floating Market', 'CS Logistik Cilegon', '00:00:00', '2019-08-19', '2019-08-19', 'Bus7552'),
(35, '919', 'Bu. Nur', '0000-00-00', 'Cikole', 'Rt/Rw 004/014, Gg Mekar Dua, Pegantungan Baru', '00:00:00', '2019-09-06', '2019-09-06', 'Bus7552'),
(36, '919', 'Bu. Nur', '0000-00-00', 'Cikole', 'Rt/Rw 004/014, Gg Mekar Dua, Pegantungan Baru', '00:00:00', '2019-09-06', '2019-09-06', 'Bus7553'),
(37, '919', 'Bp. Arif Istar ', '0000-00-00', 'Ancol Jakarta', 'Taman Krakatau Cilegon', '00:00:00', '2019-09-10', '2019-09-10', 'Bus7551'),
(38, '919', 'Bp. Arif Istar ', '0000-00-00', 'Ancol Jakarta', 'Taman Krakatau Cilegon', '00:00:00', '2019-09-10', '2019-09-10', 'Bus7508'),
(39, '919', 'Bp. Arif Istar ', '0000-00-00', 'Ancol Jakarta', 'Taman Krakatau Cilegon', '00:00:00', '2019-09-10', '2019-09-10', 'Bus7507'),
(40, '919', 'Bp. Arif Istar ', '0000-00-00', 'Ancol Jakarta', 'Taman Krakatau Cilegon', '00:00:00', '2019-09-10', '2019-09-10', 'Bus7507'),
(41, '819', 'Bp. Amin Murodi', '0000-00-00', 'Cirebon, Muria & Guci', 'Anyer', '00:00:00', '2019-08-25', '2019-08-25', 'Bus7508');

-- --------------------------------------------------------

--
-- Table structure for table `booking_truk`
--

CREATE TABLE `booking_truk` (
  `kode_booking_truk` char(5) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `tgl_booking` date NOT NULL,
  `tgl_pakai` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `id_kendaraan` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id_kendaraan` char(10) NOT NULL,
  `id_periode` char(4) NOT NULL,
  `jenis` enum('bus','truk') NOT NULL,
  `nopol` char(8) NOT NULL,
  `merk` varchar(20) NOT NULL,
  `tahun` year(4) NOT NULL,
  `ban` enum('tebal','tipis') NOT NULL,
  `oli` enum('setengah','seperempat','full') NOT NULL,
  `air_radiator` enum('setengah','seperempat','full') NOT NULL,
  `ac` enum('dingin','tidak_dingin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id_kendaraan`, `id_periode`, `jenis`, `nopol`, `merk`, `tahun`, `ban`, `oli`, `air_radiator`, `ac`) VALUES
('Bus7507', '', 'bus', 'A 7507 S', 'HINO', 2017, 'tebal', 'seperempat', 'setengah', 'tidak_dingin'),
('Bus7508', '', 'bus', 'A7508 S', 'HINO', 2017, 'tipis', 'setengah', 'setengah', 'dingin'),
('Bus7551', '', 'bus', 'A 7551 S', 'HINO', 2017, 'tebal', 'setengah', 'setengah', 'dingin'),
('Bus7552', '', 'bus', 'A 7552 S', 'HINO', 2017, '', 'setengah', 'setengah', 'dingin'),
('Bus7553', '', 'bus', 'A 7553 S', 'HINO', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9020', '', 'truk', 'A 9020 T', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9021', '', 'truk', 'A 9021 T', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9022', '', 'truk', 'A 9022 T', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9023', '', 'truk', 'A 9023 T', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9024tz', '', 'bus', 'A 9024 T', 'Nissan CWA 260', 2011, 'tipis', 'setengah', 'setengah', 'dingin'),
('Truk9025', '', 'truk', 'A 9025 T', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9158', '', 'truk', 'A 9158 Y', 'Nissan CWA 260 MX', 2015, '', 'setengah', 'setengah', 'dingin'),
('Truk9193', '', 'truk', 'A 9193 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9194', '', 'truk', 'A 9194 R', 'Nissan CWA 260u', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9195', '', 'truk', 'A 9195 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9196', '', 'truk', 'A 9196 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9197', '', 'truk', 'A 9197 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9198', '', 'truk', 'A 9198 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9240', '', 'truk', 'A 9240 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9241', '', 'truk', 'A 9241 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9242', '', 'truk', 'A 9242 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9243', '', 'truk', 'A 9243 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9244', '', 'truk', 'A 9244 R', 'Nissan CWA 260', 2017, '', 'setengah', 'setengah', 'dingin'),
('Truk9314', '', 'truk', 'A 9314 R', 'MITSUBISHI FN 527', 2014, '', 'setengah', 'setengah', 'dingin'),
('Truk9321', '', 'truk', 'A 9321 B', 'MITSUBISHI FN 527', 2013, '', 'setengah', 'setengah', 'dingin'),
('Truk9322', '', 'truk', 'A 9322 B', 'MITSUBISHI FN 527', 2013, '', 'setengah', 'setengah', 'dingin'),
('Truk9323', '', 'truk', 'A 9323 B', 'MITSUBISHI FN 527', 2013, '', 'setengah', 'setengah', 'dingin'),
('Truk9324', '', 'truk', 'A 9324 B', 'MITSUBISHI FN 527', 2013, '', 'setengah', 'setengah', 'dingin'),
('Truk9325', '', 'truk', 'A 9325 B', 'MITSUBISHI FN 527', 2013, '', 'setengah', 'setengah', 'dingin'),
('Truk9342', '', 'truk', 'A 9342 S', 'UD Trucks', 2018, '', 'setengah', 'setengah', 'dingin'),
('Truk9343', '', 'truk', 'A 9343 S', 'UD Trucks', 2018, '', 'setengah', 'setengah', 'dingin'),
('Truk9344', '', 'truk', 'A 9344 S', 'UD Trucks', 2018, '', 'setengah', 'setengah', 'dingin'),
('Truk9345', '', 'truk', 'A 9345 S', 'UD Trucks', 2018, '', 'setengah', 'setengah', 'dingin'),
('Truk9346', '', 'truk', 'A 9346 S', 'UD Trucks', 2018, '', 'setengah', 'setengah', 'dingin'),
('Truk9475', '', 'truk', 'A 9475 A', 'MITSUBISHI FN 527', 2010, '', 'setengah', 'setengah', 'dingin'),
('Truk9516', '', 'truk', 'A 9516 W', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9517', '', 'truk', '9517', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9518', '', 'truk', '9518', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9527', '', 'truk', '9527', 'HINO', 2018, '', 'setengah', 'setengah', 'dingin'),
('Truk9557', '', 'truk', '9557', 'Nissan CWA 260', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9616', '', 'truk', '9616', 'MITSUBISHI FN 527', 2011, '', 'setengah', 'setengah', 'dingin'),
('Truk9717', '', 'truk', '9717', 'Nissan CWA 260 MX', 2014, '', 'setengah', 'setengah', 'dingin'),
('Truk9842', '', 'truk', '9842', 'Nissan CWA 260 MX', 2014, '', 'setengah', 'setengah', 'dingin');

-- --------------------------------------------------------

--
-- Table structure for table `kondisi`
--

CREATE TABLE `kondisi` (
  `id_kondisi` char(4) NOT NULL,
  `kondisi` enum('Perbaikan','Baik','Rusak') NOT NULL,
  `detail` enum('> 2','<= 2','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kondisi`
--

INSERT INTO `kondisi` (`id_kondisi`, `kondisi`, `detail`) VALUES
('BK', 'Baik', '<= 2'),
('PB', 'Perbaikan', '0'),
('RK', 'Rusak', '> 2');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `id_kendaraan` char(10) DEFAULT NULL,
  `nopol` char(10) NOT NULL,
  `C1` int(5) NOT NULL,
  `C2` int(5) NOT NULL,
  `C3` int(5) NOT NULL,
  `C4` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `id_kendaraan`, `nopol`, `C1`, `C2`, `C3`, `C4`) VALUES
(1, 'Truk9024tz', 'A 7507 S', 1, 3, 3, 1),
(2, ' Truk9342', 'A 9020 T', 1, 3, 3, 1),
(3, ' Bus7553', 'A 7551 S', 1, 3, 3, 1),
(4, ' Bus7551', 'A 9021 T', 1, 3, 3, 1),
(5, 'Truk9158', '', 1, 3, 3, 1),
(6, NULL, 'Bus7507', 1, 3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_periodik`
--

CREATE TABLE `monitoring_periodik` (
  `id` int(11) NOT NULL,
  `id_kendaraan` char(10) NOT NULL,
  `ban` enum('tipis','tebal') NOT NULL,
  `oli` enum('setengah','seperempat','full') NOT NULL,
  `air_radiator` enum('setengah','seperempat','full') NOT NULL,
  `ac` enum('dingin','kurang_dingin','tidak_dingin') NOT NULL,
  `r` int(11) DEFAULT NULL,
  `v` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monitoring_periodik`
--

INSERT INTO `monitoring_periodik` (`id`, `id_kendaraan`, `ban`, `oli`, `air_radiator`, `ac`, `r`, `v`) VALUES
(1, 'Bus7507', 'tipis', 'seperempat', 'setengah', 'dingin', NULL, NULL),
(2, 'Bus7508', 'tipis', 'full', 'setengah', 'dingin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` char(4) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama`, `jabatan`) VALUES
('P101', 'Guntur', 'Kepala Admin dan Logistik'),
('P102', 'Reni', 'Admin'),
('P103', 'Rudy Hartono', 'Staff Mekanik');

-- --------------------------------------------------------

--
-- Table structure for table `penggunaan_ban`
--

CREATE TABLE `penggunaan_ban` (
  `id_penggunaan_ban` char(5) NOT NULL,
  `id_kendaraan` char(10) NOT NULL,
  `id_periode` char(4) NOT NULL,
  `id_sopir` char(4) NOT NULL,
  `tgl_pakai` date NOT NULL,
  `id_ban` char(5) NOT NULL,
  `km_pakai` int(11) NOT NULL,
  `tgl_lepas` date NOT NULL,
  `km_lepas` int(11) NOT NULL,
  `shor_over` int(11) NOT NULL,
  `km` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `shor_over2` int(11) NOT NULL,
  `ban_bekas` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penggunaan_ban`
--

INSERT INTO `penggunaan_ban` (`id_penggunaan_ban`, `id_kendaraan`, `id_periode`, `id_sopir`, `tgl_pakai`, `id_ban`, `km_pakai`, `tgl_lepas`, `km_lepas`, `shor_over`, `km`, `balance`, `shor_over2`, `ban_bekas`) VALUES
('PB001', 'Truk9020', '819', 's001', '2019-05-05', 'B0001', 41, '2019-05-08', 70, 29, -6, 107, 645, ' D7A2 K4317'),
('PB002', 'Truk9020', '819', 's001', '2019-05-05', 'B0002', 41, '2019-08-05', 70, 29, -6, 107, 645, ' D7A2 K0678'),
('PB003', 'Truk9516', '819', 's001', '2019-04-08', 'B0003', 59, '2019-08-04', 100, 41, 643, 86, 55, ' T194 7712 517'),
('PB004', 'Truk9516', '819', 's001', '2019-05-11', 'B0004', 59, '2019-08-03', 100, 41, 643, 86, 55, ' T220 4662 517'),
('PB005', 'Truk9021', '819', 's001', '2019-06-04', 'B0005', 64, '2019-08-14', 107, 42, 7, 107, 766, ' T194 7752 517'),
('PB006', 'Truk9021', '819', 's001', '2019-04-06', 'B0006', 64, '2019-08-14', 104, 40, 5, 107, 528, ' T172 5162 417'),
('PB007', 'Truk9025', '0', 's001', '2018-05-16', 'B0007', 65, '2018-10-23', 94, 29, -6, 99, 571, ' D6L3 K4731'),
('PB008', 'Truk9025', '0', 's001', '2018-05-18', 'B0008', 65, '2018-12-27', 104, 39, 4, 99, 411, ' E721 078 417'),
('PB009', 'Truk9025', '0', 's001', '2018-05-18', 'B0009', 65, '2019-01-13', 107, 41, 6, 99, 630, ' E721 140 012'),
('PB010', 'Truk9025', '0', 's001', '2018-05-26', 'B0010', 67, '2019-05-16', 123, 57, 40, 103, 1, ' D6L3 K4267'),
('PB011', 'Truk9023', '0', 's001', '2018-06-01', 'B0011', 70, '2019-04-29', 120, 51, 40, 108, 1, ' D6L3 K4267'),
('PB012', 'Truk9023', '0', 's001', '2018-06-01', 'B0012', 70, '2019-04-29', 120, 50, 40, 108, 1, ' D7A2 K4317'),
('PB013', 'Truk9197', '0', 's001', '2018-10-23', 'B0013', 94, '2019-07-21', 132, 37, 40, 108, 294, 'A821 061 341'),
('PB014', 'Truk9197', '0', 's001', '2018-10-23', 'B0014', 94, '2019-07-21', 132, 37, 40, 108, 294, 'G721 341 867'),
('PB015', 'Truk9242', '0', 's001', '2018-12-01', 'B0015', 100, '2019-04-24', 119, 19, 25, 135, 785, '5723 989 0518'),
('PB016', 'Truk9242', '0', 's001', '2018-12-01', 'B0016', 100, '2019-04-24', 119, 19, 25, 135, 785, '5723 907 0518'),
('PB017', 'Truk9242', '0', 's001', '2018-12-27', 'B0017', 104, '2019-05-16', 123, 18, 40, 108, 2, 'NO. A821 175 15'),
('PB018', 'Truk9242', '0', 's001', '2018-12-27', 'B0018', 104, '2019-10-26', 147, 42800, 2800, 108, 303, 'NO. D721 101 91'),
('PB019', 'Truk9244', '0', 's001', '2019-01-13', 'B0019', 107, '2019-10-23', 147, 40, 35, 109, 563, 'A821 264 141'),
('PB020', 'Truk9244', '1219', 's001', '2019-01-13', 'B0020', 107, '2019-10-23', 147, 40, 35, 109, 563, 'D721 120 829'),
('PB021', 'Truk9243', '0', 's001', '2019-04-24', 'B0021', 119, '2019-10-25', 0, 0, 94, 100, 0, 'T4018 7275'),
('PB022', 'Truk9243', '0', 's001', '2019-04-24', 'B0022', 119, '2019-10-25', 0, 0, 94, 0, 0, 'T4018 6931'),
('PB023', 'Bus7507', '0', 's001', '2019-04-29', 'B0023', 120, '2019-10-27', 0, 0, 108, 0, 0, '7022 813 0218'),
('PB024', 'Bus7507', '0', 's001', '2019-04-29', 'B0024', 120, '2019-10-27', 0, 0, 108, 0, 0, '7024 072 0218'),
('PB025', 'Bus7551', '0', 's001', '2019-05-16', 'B0025', 123, '2019-10-26', 0, 0, 0, 0, 0, '7016 874 0218'),
('PB026', 'Bus7551', '0', 's001', '2019-05-16', 'B0026', 123, '2019-10-26', 0, 0, 0, 0, 0, 'C110 7748 1118'),
('PB027', 'Bus7553', '0', 's001', '2019-07-21', 'B0027', 131, '2019-10-26', 0, 0, 0, 0, 0, '7022 421 0218'),
('PB028', 'Bus7553', '0', 's001', '2019-07-21', 'B0028', 132, '2019-10-26', 0, 0, 0, 0, 0, '7022 694 0218');

-- --------------------------------------------------------

--
-- Table structure for table `penggunaan_kendaraan`
--

CREATE TABLE `penggunaan_kendaraan` (
  `id_pakai` int(11) NOT NULL,
  `id_kendaraan` char(10) NOT NULL,
  `nopol` char(7) NOT NULL,
  `bulan` enum('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periodee`
--

CREATE TABLE `periodee` (
  `id_periode` char(4) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `tahun` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodee`
--

INSERT INTO `periodee` (`id_periode`, `bulan`, `tahun`) VALUES
('1019', 'Oktober', 2019),
('1119', 'November', 2019),
('1219', 'Desember', 2019),
('819', 'Agustus', 2019),
('919', 'September ', 2019);

-- --------------------------------------------------------

--
-- Table structure for table `sopir`
--

CREATE TABLE `sopir` (
  `id_sopir` char(4) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sopir`
--

INSERT INTO `sopir` (`id_sopir`, `nama`) VALUES
('s001', 'Lutfi');

-- --------------------------------------------------------

--
-- Table structure for table `sparepart`
--

CREATE TABLE `sparepart` (
  `id_maintain` char(10) NOT NULL,
  `nopol` char(10) NOT NULL,
  `id_periode` int(4) NOT NULL,
  `tanggal` date NOT NULL,
  `nomor` int(11) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `deskripsi` varchar(150) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` enum('pcs','set','mtr') NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sparepart`
--

INSERT INTO `sparepart` (`id_maintain`, `nopol`, `id_periode`, `tanggal`, `nomor`, `nama_barang`, `deskripsi`, `quantity`, `unit`, `total`) VALUES
('Mtsp01', 'A 9020 T', 0, '2019-07-10', 1746, 'VELG BELAKANG AKP 165 LOB. 8	', 'AKP NISSAN', 2, 'pcs', 2),
('Mtsp02', 'A 9020 T', 0, '2019-07-10', 1747, 'BAN RADIAL MRF S3C8 10.00 R20 GH', 'NO. 7035 865 0819/KM.516.186 - NO. 7024 419 1118', 1, 'set', 4),
('Mtsp03', 'A 9020 T', 0, '2019-07-10', 1748, 'BAN RADIAL MRF S3C8 10.00 R20 GH', 'NO. 7037 358 1219/KM.516.186 - NO. 7010 988 218', 1, 'set', 4),
('Mtsp04', 'A 9020 T', 0, '2019-07-10', 1749, 'BAN RADIAL MRF S3C8 10.00 R20 GH', 'NO. 7018 273 1219/KM.516.186 - NO. A182 321 350', 1, 'set', 4),
('Mtsp05', 'A 9020 T', 0, '2019-07-23', 1835, 'KARET REM BELAKANG SK SC80133 NISSAN', 'SK JPN SC80133', 4, 'pcs', 44),
('Mtsp06', 'A 9020 T', 0, '2019-07-23', 1836, 'KARET REM DEPAN SK SC80093R NISSAN', 'SK JPN SC80093', 4, 'pcs', 44),
('Mtsp07', 'A 9020 T', 0, '2019-07-26', 1845, 'BAN DALAM ASPIRA 10.00-20', 'BAN ASPIRA', 1, 'pcs', 350),
('Mtsp08', 'A 9020 T', 0, '2019-07-26', 1846, 'BAN PERUT ASPIRA 10.00-20', 'BAN ASPIRA', 1, 'pcs', 100),
('Mtsp09', 'A 9020 T', 0, '2019-07-27', 1849, 'BOGI SEAT TRUNION NISSAN', 'BOGI NISSAN', 1, 'pcs', 1),
('Mtsp10', 'A 9020 T', 0, '2019-07-27', 1850, 'RING BOGI SEAT TRUNION NISSAN', 'BOGI NISSAN', 2, 'pcs', 130),
('Mtsp100', 'Bus7507', 719, '2020-02-03', 16, 'VELG BELAKANG', 'Ganti tutup', 1, 'pcs', 1),
('Mtsp11', 'A 9020 T', 0, '2019-07-27', 1851, 'SHIM BOGI TRUNION NISSAN', 'BOGI NISSAN 5555-90003', 7, 'pcs', 245),
('Mtsp12', 'A 9020 T', 0, '2019-07-27', 1852, 'SEAL BOGI SEAT TRUNION NISSAN', 'BOGI NISSAN', 1, 'pcs', 75),
('Mtsp13', 'A 9020 T', 0, '2019-07-27', 1853, 'BEHEL PER BELAKANG NISSAN', 'BELTON', 2, 'pcs', 370),
('Mtsp14', 'A 9020 T', 0, '2019-07-27', 1854, 'BAUT CENTER PER BELAKANG NISSAN', 'BELTON', 1, 'pcs', 50),
('Mtsp15', 'A 9020 T', 0, '2019-07-29', 1878, 'BAUT RODA DEPAN NISSAN LH', 'HWC', 2, 'pcs', 120),
('Mtsp16', 'A 9021 T', 0, '2019-07-01', 1709, 'KARET DINGDONG HOP', 'HOP NISSAN', 5, 'pcs', 750),
('Mtsp17', 'A 9021 T', 0, '2019-07-01', 1710, 'BAUT DINGDONG PANJANG', 'DINGDONG', 2, 'pcs', 80),
('Mtsp18', 'A 9021 T', 0, '2019-07-01', 1711, 'BAUT DINGDONG PENDEK', 'DINGDONG', 4, 'pcs', 100),
('Mtsp19', 'A 9021 T', 0, '2019-07-01', 1712, 'LAMPU BAK LED 350', 'LED 350 LEBAR', 2, 'pcs', 70),
('Mtsp20', 'A 9021 T', 0, '2019-07-22', 1830, 'KARET DINGDONG HOP', 'HOP', 2, 'pcs', 300),
('Mtsp21', 'A 9021 T', 0, '2019-07-22', 1831, 'PIPA REM NISSAN', 'PIPA REM', 1, 'mtr', 35),
('Mtsp22', 'A 9021 T', 0, '2019-07-24', 1838, 'BOHLAMP 24V K-1/K-2', 'LAMPU', 2, 'pcs', 6),
('Mtsp23', 'A 9021 T', 0, '2019-07-24', 1839, 'BOHLAMP 24V K-1/K-2', 'LAMPU', 2, 'pcs', 6),
('Mtsp24', 'Truk9024', 0, '2019-07-01', 1704, 'BAUT RODA BELAKANG NISSAN JUMBO RH', 'HWC NISSAN', 2, 'pcs', 170),
('Mtsp25', 'A 9025 T', 0, '2019-07-02', 1718, 'BEARING RODA BELAKANG DALAM NISSAN', 'BEARING NTN 32218', 1, 'pcs', 750),
('Mtsp26', 'A 9025 T', 0, '2019-07-02', 1719, 'KAMPAS REM BELAKANG NISSAN', 'RCA NISSAN', 4, 'pcs', 175),
('Mtsp27', 'A 9025 T', 0, '2019-07-20', 1803, 'PER BELAKANG NISSAN NO. 8', 'UNIVERSAL', 1, 'pcs', 185),
('Mtsp28', 'A 9025 T', 0, '2019-07-20', 1804, 'PER BELAKANG NISSAN No. 7', 'UNIVERSAL', 1, 'pcs', 290),
('Mtsp29', 'A 9025 T', 0, '2019-07-20', 1805, 'BEHEL PER BELAKANG NISSAN', 'BELTON', 2, 'pcs', 370),
('Mtsp30', 'A 9025 T', 0, '2019-07-20', 1806, 'BAUT RODA BELAKANG NISSAN HWC LH', 'HWC KECIL LH', 1, 'pcs', 70),
('Mtsp31', 'A 9025 T', 0, '2019-07-20', 1807, 'SEAL RODA BELAKANG DALAM NISSAN G', 'NISSAN 90060D', 1, 'pcs', 85),
('Mtsp32', 'A 9516 W', 0, '2019-07-02', 1720, 'FILTER SOLAR ATAS NISSAN', 'ASTRA 16403-NY001D', 1, 'pcs', 55),
('Mtsp33', 'A 9516 W', 0, '2019-07-02', 1721, 'FILTER SOLAR BAWAH NISSAN', 'ASTRA 16403-97001D', 1, 'pcs', 65),
('Mtsp34', 'A 9516 W', 0, '2019-07-02', 1722, 'BOHLAMP SPEEDOMETER 24 V', 'LAMPU', 6, 'pcs', 12),
('Mtsp35', 'A 9516 W', 0, '2019-07-02', 1723, 'BOHLAMP 24 V K-1/K-2', 'LAMPU', 3, 'pcs', 9),
('Mtsp36', 'A 9516 W', 0, '2019-07-02', 1724, 'BOHLAMP 24 V K-1/K-2', 'LAMPU', 2, 'pcs', 6),
('Mtsp37', 'A 9516 W', 0, '2019-07-16', 1781, 'BOOSTER KOPLING BAWAH NISSAN JKC', 'JKC JAPAN', 1, 'pcs', 2),
('Mtsp38', 'A 9516 W', 0, '2019-07-02', 1782, 'BOHLAMP 24V K-1/K-2', 'LAMPU', 3, 'pcs', 9),
('Mtsp39', 'A 9516 W', 0, '2019-07-18', 1797, 'AIR ACCU DEMIN', 'ACCU', 2, 'pcs', 10),
('Mtsp40', 'A 9516 W', 0, '2019-07-18', 1798, 'BOHLAMP 24V K-1/K-2', 'LAMPU', 2, 'pcs', 6),
('Mtsp41', 'A 9516 W', 0, '2019-07-18', 1799, 'VELG BELAKANG AKP 165 LOB. 8', 'AKP NISSAN', 1, 'pcs', 1),
('Mtsp42', 'A 9516 W', 0, '2019-07-21', 1817, 'WHEEL CYLINDER FRONT NISSAN', '41100-90208', 2, 'pcs', 900),
('Mtsp43', 'A 9516 W', 0, '2019-07-21', 1818, 'WHEEL CYLINDER FRONT NISSAN', '41100-90207', 2, 'pcs', 900),
('Mtsp44', 'A 9516 W', 0, '2019-07-21', 1819, 'BEARING RODA DEPAN NISSAN JPN', 'JPN 30313', 1, 'pcs', 475),
('Mtsp45', 'A 9516 W', 0, '2019-07-29', 1876, 'BAUT RODA DEPAN NISSAN LH', 'HWC', 2, 'pcs', 120),
('Mtsp46', 'A 9516 W', 0, '2019-07-29', 1887, 'ACCU YUASA BATTERY NS70 + AIR', 'YUASA BATTERY', 2, 'pcs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `role` enum('admin','kaadm_logistik','staff_mekanik') NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(45) NOT NULL,
  `id_pegawai` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `role`, `password`, `email`, `id_pegawai`) VALUES
(1, 'kaop_bus', '', '4ccf1890ac807664c51dd037658f2879', '', ''),
(2, 'operator', '', '4b583376b2767b923c3e1da60d10de59', 'aisahicaa@email.unikom.ac.id', '2'),
(3, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'kosong', 'P101'),
(4, 'kaadm', 'kaadm_logistik', '3552662c99118a065a62ab53566603ca', '-', 'P102'),
(5, 'staff', 'staff_mekanik', '1253208465b1efa876f982d8a9e73eef', '--', 'P101'),
(6, 'sss', 'admin', '666', '-', 'P102');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ban`
--
ALTER TABLE `ban`
  ADD PRIMARY KEY (`id_ban`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`kode_booking`),
  ADD KEY `id_kendaraan` (`id_kendaraan`),
  ADD KEY `periode` (`id_periode`);

--
-- Indexes for table `booking_truk`
--
ALTER TABLE `booking_truk`
  ADD PRIMARY KEY (`kode_booking_truk`),
  ADD KEY `id_kendaraan` (`id_kendaraan`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `kondisi`
--
ALTER TABLE `kondisi`
  ADD PRIMARY KEY (`id_kondisi`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`),
  ADD KEY `id_kendaraan` (`id_kendaraan`);

--
-- Indexes for table `monitoring_periodik`
--
ALTER TABLE `monitoring_periodik`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kendaraan` (`id_kendaraan`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `penggunaan_ban`
--
ALTER TABLE `penggunaan_ban`
  ADD PRIMARY KEY (`id_penggunaan_ban`),
  ADD KEY `id_kendaraan` (`id_kendaraan`),
  ADD KEY `id_sopir` (`id_sopir`),
  ADD KEY `id_ban` (`id_ban`);

--
-- Indexes for table `periodee`
--
ALTER TABLE `periodee`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `sopir`
--
ALTER TABLE `sopir`
  ADD PRIMARY KEY (`id_sopir`);

--
-- Indexes for table `sparepart`
--
ALTER TABLE `sparepart`
  ADD PRIMARY KEY (`id_maintain`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `monitoring_periodik`
--
ALTER TABLE `monitoring_periodik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraan` (`id_kendaraan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`id_periode`) REFERENCES `periodee` (`id_periode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `booking_truk`
--
ALTER TABLE `booking_truk`
  ADD CONSTRAINT `booking_truk_ibfk_1` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraan` (`id_kendaraan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD CONSTRAINT `kriteria_ibfk_1` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraan` (`id_kendaraan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `monitoring_periodik`
--
ALTER TABLE `monitoring_periodik`
  ADD CONSTRAINT `monitoring_periodik_ibfk_1` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraan` (`id_kendaraan`);

--
-- Constraints for table `penggunaan_ban`
--
ALTER TABLE `penggunaan_ban`
  ADD CONSTRAINT `penggunaan_ban_ibfk_2` FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraan` (`id_kendaraan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penggunaan_ban_ibfk_3` FOREIGN KEY (`id_sopir`) REFERENCES `sopir` (`id_sopir`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

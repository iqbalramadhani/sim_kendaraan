
<h1 class="h4 mb-2 text-gray-800">Data Kondisi</h1>
<div class="card shadow mb-4">
        <div class="card-body">
          <p align="right">

          <a href="#" data-toggle="modal" data-target="#Add" class="btn btn-danger">Tambah </a>
          <?php include 'add-kondisi.php';?>
          </p>

          <div class="table-responsive">
            <table class="table table-bordered" id="example" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">ID Kondisi</th>
                  <th scope="col">Kondisi</th>
                  <th scope="col">Detail</th>
                  <th scope="col">Aksi</th>

                </tr>
              </thead>

              <tbody>
                <?php
                        if( ! empty($DataKondisi)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                          $i = 1;
                          foreach($DataKondisi as $data){
                            // echo "<tr>
                            // <td>".$data->id_soal."</td>
                            // <td>".$data->kode_soal."</td>
                            // <td>".$data->id_matpel."</td>
                            // <td>".$data->merk."</td>
                            // <td>".$data->pembahasan."</td>
                            // <td>".$data->bobot."</td>
                            // <td>
                            // <a href=\"".base_url('index.php/inputsoal')."\"> class=\"btn btn-info">Edit Data</a>
                            // <a href="<?php echo base_url('index.php/inputsoal');
                           /* " class="btn btn-info">Hapus Data</a>
                             </tr>;
                           */

                            echo
                            '<tr>
                            <td>'.$i++.'</td>
                              <td>'.$data->id_kondisi.'</td>
                             <td>'.$data->kondisi.'</td>
                             <td>'.$data->detail.'</td>


                              <td>
                                <a href="#" data-toggle="modal" data-target="#Edit'.$data->id_kondisi.'"
                                class="btn btn-info">Edit
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  </a>

                                <a href="#" data-toggle="modal" data-target="#Delete'.$data->id_kondisi.'" class="btn btn-danger">Delete </a>
                              </td>
                            </tr>';
                           include 'edit-kondisi.php';
                          include 'delete-kondisi.php';

                            $i++;
                          }
                        }else{ // Jika data  kosong
                          echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                        }
                        ?>
                      </tbody>
                    </table>

                  </div>
                </div>

                <script>
                    $(document).ready(function() {
                        $('#example').DataTable({
                            "scrollX": true
                        });
                    });
                </script>


<!-- The Modal -->
<div class="modal" id="Edit<?php echo $data->id_user;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaadm_logistik/pengguna'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">
          
          <div class="form-group">
            <label>Nama pengguna <span class="text-danger">*</span></label>
            <input type="text" name="username" id="username" value="<?php echo $data->username; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Kata Sandi<span class="text-danger">*</span></label>
            <input type="password" name="password" id="password" value="<?php echo $data->password; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>E-mail<span class="text-danger">*</span></label>
            <input type="text" name="email" id="email" value="<?php echo $data->email; ?>" class="form-control">
          </div>



          <input type="hidden" value="<?php echo $data->id_user; ?>" name="id_user">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="Edit<?php echo $data->id_pegawai;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaadm_logistik/pegawai'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">
          
          <div class="form-group">
            <label>ID Pegawai <span class="text-danger">*</span></label>
            <input type="text" name="id_pegawai" id="id_pegawai" value="<?php echo $data->id_pegawai; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Nama<span class="text-danger">*</span></label>
            <input type="text" name="nama" id="nama" value="<?php echo $data->nama; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Jabatan<span class="text-danger">*</span></label>
            <input type="text" name="jabatan" id="jabatan" value="<?php echo $data->jabatan; ?>" class="form-control">
          </div>



          <input type="hidden" value="<?php echo $data->id_pegawai; ?>" name="id_pegawai">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>
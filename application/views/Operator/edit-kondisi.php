
<!-- The Modal -->
<div class="modal" id="Edit<?php echo $data->id_kondisi;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Operator/kondisi'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">

          <div class="form-group">
            <label>ID Kondisi <span class="text-danger">*</span></label>
            <input type="text" name="id_kondisi" id="id_kondisi" value="<?php echo $data->id_kondisi; ?>" class="form-control">
          </div>
          
          <div class="form-group">
            <label>Kondisi<span class="text-danger">*</span></label>
            <input type="text" name="kondisi" id="kondisi" value="<?php echo $data->kondisi; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Detail<span class="text-danger">*</span></label>
            <input type="text" name="detail" id="detail" value="<?php echo $data->detail; ?>" class="form-control">
          </div>



          <input type="hidden" value="<?php echo $data->id_kondisi; ?>" name="id_kondisi">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>
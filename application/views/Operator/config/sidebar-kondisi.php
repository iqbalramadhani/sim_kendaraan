<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3"> Kepala Operator</div>
  </a>
      <hr class="sidebar-divider my-0">


            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/Operator')?>">
                <span class="menu-title">Data Kendaraan</span>
                <!-- <i class="mdi mdi-format-list-numbered menu-icon"></i> -->
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="<?php echo base_url('index.php/Operator/kondisi')?>">
                <span class="menu-title">Data Kondisi</span>
                <!-- <i class="mdi mdi-account-key menu-icon"></i> -->
              </a>
            </li>

            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
          </ul>

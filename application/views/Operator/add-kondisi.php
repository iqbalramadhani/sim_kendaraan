
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah kondisi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Operator/Kondisi'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">

        
          <div class="form-group">
            <label>id kondisi <span class="text-danger">*</span></label>
            <input type="text" name="id_kondisi" id="id_kondisi" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>Kondisi <span class="text-danger">*</span></label>
            <input type="text" name="kondisi" id="kondisi" class="form-control" required="">
            
          </div>
          <div class="form-group">
            <label>Detail<span class="text-danger">*</span></label>
            <input type="text" name="detail" id="detail" class="form-control" required="">
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>
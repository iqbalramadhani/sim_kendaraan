
<!-- The Modal -->
<div class="modal" id="Delete<?php echo $data->id_kondisi;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Delete Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Operator/Kondisi'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">

          <p>Apakah Anda yakin ingin menghapus data ini <b>"<?php echo '['.$data->id_kondisi.']'; ?></b></p>
          <input type="hidden" value="<?php echo $data->id_kondisi; ?>" name="id_kondisi">
          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="delete" class="btn btn-danger" name="delete" >Delete</button>
          
        </div>
      </form>

    </div>
  </div>
</div>
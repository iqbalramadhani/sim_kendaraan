<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIM Kendaraan</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url('assets/css/sb-admin-2.min.css') ?>" rel="stylesheet">
  <script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js') ?>"></script>
  <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">

        </div>
        <div class="sidebar-brand-text mx-3"> Kepala Admin </div>
      </a>
      <hr class="sidebar-divider my-0">


      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url('index.php/Beranda/') ?>">
          <span>Data Penggunaan Kendaraan</span>
          <!-- <i class="nc-icon nc-bell-55"></i> -->
        </a>
      </li>

      <div class="sidebar-heading">
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Rekomendasi</span>
        </a>
        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="<?php echo base_url('index.php/Kaadm_Logistik/kriteria/') ?>">Kriteria</a>
            <a class="collapse-item " href="<?php echo base_url('index.php/Saw/') ?>">Hitung Kriteria</a>
          </div>
        </div>
      </li>
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>

    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Navbar -->
        <?php include_once "config/navbar.php"; ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="card shadow mb-4">
            <div class="container-fluid">
              <div class="card-header">
                Grafik
              </div>
              <div class="form-group">
                <form action="getPeriode">
                  <label>Bulan <span class="text-danger">*</span></label>
                  <select name="id_periode" class="form-control" id="periode">
                    <?php
                    if (!empty($DataPeriode)) {
                      foreach ($DataPeriode as $key) {
                        // if ($key->id_periode == $data->id_periode) {
                        //   echo ' <option value="' . $key->id_periode . '" selected>' . $key->bulan . '</option>';
                        // } else {
                        echo ' <option value="' . $key->id_periode . '">' . $key->bulan . '</option>';
                        // }
                      }
                    }
                    ?>
                  </select>
              </div>
              <canvas id="myChart">

              </canvas>
              <?php
              //Inisialisasi nilai variabel awal
              $nama_jurusan = "";
              $jumlah = null;
              foreach ($hasil as $item) {
                $kendaraan = $item->id_kendaraan;
                $nama_jurusan .= "'$kendaraan'" . ", ";
                $jum = $item->total;
                $jumlah .= "$jum" . ", ";
              }
              ?>
              <script>
                var ctx = document.getElementById('myChart').getContext('2d');

                var chart = new Chart(ctx, {
                  // The type of chart we want to create
                  type: 'bar',
                  // The data for our dataset
                  data: {
                    labels: [<?php echo $nama_jurusan; ?>],
                    datasets: [{
                      label: 'Data Penggunaan Kendaraan ',
                      backgroundColor: ['rgb(255, 99, 132)', 'rgba(56, 86, 255, 0.87)', 'rgb(60, 179, 113)', 'rgb(175, 238, 239)'],
                      borderColor: ['rgb(255, 99, 132)'],
                      data: [<?php echo $jumlah; ?>]
                    }]
                  },
                  // Configuration options go here
                  options: {
                    scales: {
                      yAxes: [{
                        ticks: {
                          beginAtZero: true
                        }
                      }]
                    }
                  }
                });
              </script>
            </div>
          </div>
        </div>

        <?php include_once "config/footer.php"; ?>
      </div>
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!--   Core JS Files   -->
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url('assets/js/sb-admin-2.min.js') ?>"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url('assets/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url('assets/js/demo/datatables-demo.js') ?>"></script>

  <!-- Chart JS -->
  <script src="<?php echo base_url('assets/js/plugins/chartjs.min.js') ?>"></script>
  <!--  Notifications Plugin    -->

  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      // demo.initChartsPages();


      function updateChart() {

            var periodeValue = $("#periode").val();
            var formData = new FormData();
            formData.append('periode', periodeValue);
             $.ajax({
               type: 'POST',
               url: "http://localhost/sim_kendaraan/index.php/Beranda/updateDataByPeriode",
               data: formData,
               contentType: false,
               processData: false,
               success: function(data) {
                   console.log('success')
                   console.log(data.result.length)

                   var newData = [];

                   for (var i = 0; i < data.result.length; i++) {

                    newData.push(data.result[i]['total']);

                   }






                     // console.log($jumlah);
                     var ctx = document.getElementById('myChart').getContext('2d');

                     var chart = new Chart(ctx, {
                       // The type of chart we want to create
                       type: 'bar',
                       // The data for our dataset
                       data: {
                         labels: [<?php echo $nama_jurusan; ?>],
                         datasets: [{
                           label: 'Data Penggunaan Kendaraan ',
                           backgroundColor: ['rgb(255, 99, 132)', 'rgba(56, 86, 255, 0.87)', 'rgb(60, 179, 113)', 'rgb(175, 238, 239)'],
                           borderColor: ['rgb(255, 99, 132)'],
                           data: newData
                         }]
                       },
                       // Configuration options go here
                       options: {
                         scales: {
                           yAxes: [{
                             ticks: {
                               beginAtZero: true
                             }
                           }]
                         }
                       }
                     });




               },
                error: function (data) {
                  console.log('error')
                  console.log(data)
                 }
             });
      }

      // ketika dropdown bulan di ganti
      $('#periode').on('change', updateChart)



    });
  </script>
</body>

</html>

<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Kriteria</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaadm_logistik/'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>Nomor Polisi<span class="text-danger">*</span></label>
            <select name="nopol" class="form-control" id="">
            <?php
            if(!empty($DataKendaraan)){
            foreach ($DataKendaraan as $key) {
           if($key->id_kendaraan == $data->id_kendaraan){
            echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->nopol.'</option>';
          }else{
                echo ' <option value="'.$key->id_kendaraan.'">'.$key->nopol.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>Ban<span class="text-danger">*</span></label>
            <select name="C1" id="C1" class="form-control">
              <option value="1">Ban Menipis</option>
              <option value="3">Ban Tebal</option>
            </select>
          </div>
          <div class="form-group">
            <label>OLI<span class="text-danger">*</span></label>
            <select name="C2" id="C2" class="form-control">
              <option value="3">Seperempat</option>
              <option value="2">Setengah</option>
              <option value="1">Full</option>
            </select>
          </div>
          <div class="form-group">
            <label>Air Radiator<span class="text-danger">*</span></label>
            <select name="C3" id="C3" class="form-control">
              <option value="3">Seperempat</option>
              <option value="2">Setengah</option>
              <option value="1">Full</option>
            </select>
          </div>

          <div class="form-group">
            <label>AC <span class="text-danger">*</span></label>
            <select name="C4" id="C4" class="form-control" required="">
            <option value="1">Dingin</option>
            <option value="2">Tidak Dingin</option>
            </select>
          </div>


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="modal" id="Edit<?php echo $data->id_kriteria;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaadm_logistik/'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>id_kriteria<span class="text-danger">*</span></label>
            <input type="text" name="id_kriteria" id="id_kriteria" value="<?php echo $data->id_kriteria; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>Nomor Polisi <span class="text-danger">*</span></label>
            <select name="nopol" class="form-control" id="">
            <?php
            if(!empty($DataKendaraan)){
            foreach ($DataKendaraan as $key) {
           if($key->nopol == $data->nopol){
            echo ' <option value="'.$key->nopol.'" selected>'.$key->nopol.'</option>';
          }else{
                echo ' <option value="'.$key->nopol.'">'.$key->nopol.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>Ban <span class="text-danger">*</span></label>
            <select name="C1" id="C1" value="<?php echo $data->C1; ?>" class="form-control" >
              <option value="1">Ban Menipis</option>
              <option value="3">Ban Tebal</option>
            </select>
          </div>

          <div class="form-group">
            <label>Oli<span class="text-danger">*</span></label>
            <select name="C2" id="C2" value="<?php echo $data->C2; ?>" class="form-control" >
              <option value="3">Seperempat</option>
              <option value="2">Setengah</option>
              <option value="1">Full</option>
            </select>
          </div>

          <div class="form-group">
            <label>Air Radiator<span class="text-danger">*</span></label>
            <select name="C3" id="C3" value="<?php echo $data->C3; ?>" class="form-control" >
              <option value="3">Seperempat</option>
              <option value="2">Setengah</option>
              <option value="1">Full</option>
            </select>
          </div>
          <div class="form-group">
            <label>AC <span class="text-danger">*</span></label>
            <select name="C4" id="C4" value="<?php echo $data->C4; ?>" class="form-control" >
              <option value="1">Dingin</option>
              <option value="2">Tidak Dingin</option>
              </select>
          </div>
          </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>

<h1 class="h4 mb-2 text-gray-800">Data Kriteria</h1>
<div class="card shadow mb-4">
  <div class="card-body">
    <p align="right">

      <p align="right">

        <a href="#" data-toggle="modal" data-target="#Addnew" class="btn btn-danger">Tambah Kriteria </a>
        <?php $this->load->view('Kaadm/add-new'); ?>
      </p>

      <div class="table-responsive">
        <table class="table table-bordered" id="example" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nomor Polisi</th>
              <th scope="col">Ban</th>
              <th scope="col">Oli</th>
              <th scope="col">Air Radiator</th>
              <th scope="col">AC</th>
              <th scope="col">Aksi</th>

            </tr>
          </thead>

          <tbody>
            <?php
            if (!empty($DataKriteria)) { // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
              $i = 1;
              foreach ($DataKriteria as $data) {
                // echo "<tr>
                // <td>".$data->id_soal."</td>
                // <td>".$data->kode_soal."</td>
                // <td>".$data->id_matpel."</td>
                // <td>".$data->merk."</td>
                // <td>".$data->pembahasan."</td>
                // <td>".$data->bobot."</td>
                // <td>
                // <a href=\"".base_url('index.php/inputsoal')."\"> class=\"btn btn-info">Edit Data</a>
                // <a href="<?php echo base_url('index.php/inputsoal');
                /* " class="btn btn-info">Hapus Data</a>
                             </tr>;
                           */

                echo
                  '<tr>
                            <td>' . $i++ . '</td>
                            <td>' . $data->nopol . '</td>
                              <td>' . $data->C1 . '</td>
                              <td>' . $data->C2 . '</td>
                              <td>' . $data->C3 . '</td>
                              <td>' . $data->C4 . '</td>
                              <td>
                              <a href="#" data-toggle="modal" data-target="#Edit' . $data->id_kriteria . '"
      class="btn btn-info">Edit
      <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  </a>

      <a href="#" data-toggle="modal" data-target="#Delete' . $data->id_kriteria . '" class="btn btn-danger">Delete </a>
      </td>
      </tr>';

                include 'edit-kriteria.php';
                include 'delete-kriteria.php';

                $i++;
              }
            } else { // Jika data  kosong
              echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
            }
            ?>
          </tbody>
        </table>

      </div>
  </div>

  <script>
    $(document).ready(function() {
      $('#example').DataTable({
        "scrollX": true
      });
    });
  </script>
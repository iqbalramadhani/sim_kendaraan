<?php
//print_r($kriteria);
// print_r($V);
// foreach ($V as $d) {
//   $a[] = $d;
//   // arsort($a);
// }
// asort($a);
// print_r($a);
// foreach ($a as $a1) {
//   $index[] = array_search($a1, $a);
//   // echo $index;
// }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIM Kendaraan</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url('assets/css/sb-admin-2.min.css')?>" rel="stylesheet">
   <script src="<?php echo base_url('assets/js/core/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
   <script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
   <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once $this->data['sidebar']; ?>

    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
      <!-- Navbar -->
      <?php include_once "config/navbar.php"; ?>

      <!-- Begin Page Content -->
      <div class="container-fluid">

<h1 class="h4 mb-2 text-gray-800">Perhitungan Kriteria</h1>
<div class="card shadow mb-4">
                   <?php
                  if ($hasil) {
                  ?>
                  <div class="table-responsive">
                    <br><br>
                    <table class="table table-bordered" id="example" width="100%" cellspacing="0">
                      <thead>
                        <tr>

                          <b>
                            <th rowspan="2">Alternatif</th>
                            <th colspan="4">Kriteria</th>
                          </b>
                        </tr>
                        <tr>
                          <td scope="col">C1</td>
                          <td scope="col">C2</td>
                          <td scope="col">C3</td>
                          <td scope="col">C4</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($kriteria as $q) {
                        ?>
                          <tr>
                            <td scope="col"><?= $q->nopol; ?></td>
                            <td scope="col"><?= $q->C1; ?></td>
                            <td scope="col"><?= $q->C2; ?></td>
                            <td scope="col"><?= $q->C3; ?></td>
                            <td scope="col"><?= $q->C4; ?></td>
                          </tr>
                        <?php
                        }
                        ?>

                      </tbody>
                    </table>
                  </thead>
                </table>


<br><br><br>
                    <!-- <h1>Matrix X</h1>
                    <table class="table" border="1">
                      <thead>
                        <tr>
                          <td rowspan="<?= count($x) + 1; ?>">X</td>
                        </tr>
                        <?php
                        foreach ($x as $x1) {
                        ?>
                          <tr>
                            <td scope="col"><?= $x1[0]; ?></td>
                            <td scope="col"><?= $x1[1]; ?></td>
                            <td scope="col"><?= $x1[2]; ?></td>
                            <td scope="col"><?= $x1[3]; ?></td>
                          </tr>
                        <?php
                        }
                        ?>
                      </thead>
                    </table>

                    <h1>Nilai r</h1>
                    <table class="table" id="example">
                      <tbody>
                        <?php
                        $no = 1;
                        foreach ($r as $r1) {
                        ?>
                          <tr>
                            <td><?= "r" . $no . "1 " . number_format($r1[0], 2, ',', ''); ?></td>
                          </tr>
                          <tr>
                            <td><?= "r" . $no . "2 " . number_format($r1[1], 2, ',', ''); ?></td>
                          </tr>
                          <tr>
                            <td><?= "r" . $no . "3 " . number_format($r1[2], 2, ',', ''); ?></td>
                          </tr>
                          <tr>
                            <td><?= "r" . $no . "4 " . number_format($r1[3], 2, ',', ''); ?></td>
                          </tr>
                        <?php
                          $no++;
                        }
                        ?>
                      </tbody>
                    </table> -->
                    <?php
                    foreach ($V as $d) {
                      $a[] = $d;
                    }
                    asort($a);
                    foreach ($a as $a1) {
                      $index[] = array_search($a1, $a);
                    }
                    ?>
                    <h1>HASIL</h1>
                    <table class="table">
                      <thead>
                        <th>V maks</th>
                        <th>Nomor Polisi</th>
                        <th> Rekomendasi </th>
                      </thead>
                      <tbody>
                        <td><?= $a[0]; ?></td>
                        <td><?= $kriteria[$index[0]]->nopol; ?></td>
                        <td>harus diperbaiki</td>
                        <?php
                        $no = 1;

                        for ($i = 1; $i < sizeof($a); $i++) {
                        ?>
                          <tr>
                            <!--<td>  <?= "V" . $no . " " . $v; ?></td> -->
                            <td>V <?php echo ($i + 1); ?></td>
                            <td> <?php echo $kriteria[$index[$i]]->nopol; ?> </td>
                            <td>Kendaraan diperbaiki ke <?= $i + 1; ?></td>
                          </tr>
                        <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  <?php
                  }
                  ?>
                </form>
            </div>
          </div>
</div>


        <?php include_once "config/footer.php"; ?>
      </div>
      <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
    <!--   Core JS Files   -->
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assets/js/sb-admin-2.min.js') ?>"></script>

    <!-- Page level plugins -->
    <script src="<?php echo base_url('assets/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>

    <!-- Page level custom scripts -->
    <script src="<?php echo base_url('assets/js/demo/datatables-demo.js') ?>"></script>

    <!-- Chart JS -->
    <script src="<?php echo base_url('assets/js/plugins/chartjs.min.js') ?>"></script>
    <!--  Notifications Plugin    -->

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
        demo.initChartsPages();
      });
    </script>
</body>

</html>
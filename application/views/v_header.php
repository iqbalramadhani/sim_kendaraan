<html>
<body>
  <head>

  <title><?php echo $title; ?></title>
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<header>
<?php
  if(!isset($_GET['page'])){
    $page = "beranda";
  }else{
  $page = $_GET['page'];}
?>
<style>
.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
  background-color:#BBDEFB  !important;
  color:#000000 !important;
}
.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
  color:#000000 !important;
  background-color:#dddddd!important;}
  .navbar-default .navbar-nav > li > a {
  color:#ffffff !important;}
 .navbar{
  background-color: #0D47A1 !important;
  border:none !important;
 }
</style>

  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><font color='#FFFFFF'>TRITRONIK</font></a>
    </div>

 <ul class="nav navbar-nav">
    <li class="<?=($page=='beranda')?'active':'';?>"><a href="<?php echo base_url().'index.php/beranda/getRecord?page=beranda';?>"><i class="fa fa-fw fa-home"></i>BERANDA</a></li>
      <li class="<?=($page=='pangan')?'active':'';?>"><a href="<?php echo base_url().'index.php/login?page=pangan';?>"><span class="glyphicon glyphicon-file"></span>PANGAN</a></li>

    </ul>
    <ul class="nav navbar-nav navbar-right">

        <li><a href="<?=base_url();?>beranda/logout"><span class="glyphicon glyphicon-log-out"></span> LOGOUT</a></li>
      </ul>


  </div>
</nav>
</div>
</header>
 <script type="text/javascript" src="<?=base_url();?>assets/jquery/jquery-3.2.1.min.js"></script>

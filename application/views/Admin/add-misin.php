
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Misin</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Monitor_Rutin/');?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
              <label>Id Kendaraan <span class="text-danger">*</span></label>
              <select name="id_kendaraan" class="form-control" id="">
              <?php
              if(!empty($DataKendaraan)){
              foreach ($DataKendaraan as $key) {
             if($key->id_kendaraan == $data->id_kendaraan){
              echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->nopol.'</option>';
            }else{
                  echo ' <option value="'.$key->id_kendaraan.'">'.$key->nopol.'</option>';
                  }
                }
              }
               ?>
              </select>
            </div>

<div class="form-group">
  <label>Oli <span class="text-danger">*</span></label>
  <select name="oli" id="oli" class="form-control">
    <option value="setengah">Setengah</option>
    <option value="seperempat">Seperempat</option>
    <option value="full">Full</option>
  </select>
</div>
<div class="form-group">
  <label>Ban <span class="text-danger">*</span></label>
  <select name="ban" id="ban" class="form-control">
    <option value="tipis">Ban Menipis</option>
    <option value="tebal">Ban Tebal</option>
  </select>
</div>

<div class="form-group">
  <label>Air Radiator <span class="text-danger">*</span></label>
  <select name="air_radiator" id="air_radiator" class="form-control">
    <option value="setengah">Setengah</option>
    <option value="seperempat">Seperempat</option>
    <option value="full">Full</option>
  </select>
</div>
<div class="form-group">
  <label>AC <span class="text-danger">*</span></label>
  <select name="ac" id="ac" class="form-control">
    <option value="dingin">Dingin</option>
    <option value="tidak_dingin">Tidak Dingin</option>
  </select>
</div>
</div>

<!-- Modal footer -->
<div class="modal-footer">
  <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
  <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
</div>
</form>

</div>
</div>
</div>

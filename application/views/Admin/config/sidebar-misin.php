
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Kepala Admin </div>
  </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url('index.php/Kaadm_logistik/kendaraan/')?>">
                    <span class="menu-title">Data Kendaraan</span>
                    <!-- <i class="mdi mdi-format-list-numbered menu-icon"></i> -->
                  </a>
      </li>
      <li class="nav-item">
         <a class="nav-link"  href="<?php echo base_url('index.php/Kaadm_logistik')?>">
           <span>Data Penggunaan Sparepart</span>
           <i class="fas fa-fw fa-tachometer-alt"></i>
         </a>
       </li>
       <li class="nav-item" >
         <a class="nav-link" href="<?php echo base_url('index.php/Kaadm_logistik/monitor_ban')?>">
           <span>Data Penggunaan Ban</span>
           <!-- <i class="nc-icon nc-bell-55"></i> -->
         </a>
       </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/Kaadm_logistik/pengguna')?>">
                <span >Data Pengguna</span>
                <!-- <i class="nc-icon nc-single-02"></i> -->
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/Kaadm_logistik/pegawai')?>">
                <span >Data Pegawai</span>
                <i class="fas fa-fw fa-chart-area"></i>
              </a>
            </li>

            <div class="sidebar-heading">
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item active">
              <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Rekomendasi</span>
              </a>
              <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header"></h6>
                  <a class="collapse-item" href="<?php echo base_url('index.php/Kaadm_Logistik/kriteria/')?>">Kriteria</a>
                  <a class="collapse-item active" href="<?php echo base_url('index.php/Saw/')?>">Hitung Kriteria</a>
                </div>
              </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Penggunaan Kendaraan</span>
              </a>
              <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header"></h6>
                  <a class="collapse-item" href="<?php echo base_url('index.php/Beranda/grafik/')?>">Grafik</a>
                  <a class="collapse-item" href="<?php echo base_url('index.php/Beranda/')?>">Detail </a>

                </div>
              </div>
            </li>
            <!-- <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url('index.php/Saw/')?>">
                  <span >Rekomendasi</span>
                  <i class="fas fa-fw fa-chart-area"></i>
                </a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/Beranda/grafik/')?>">
                <span >Grafik Penggunaan Kendaraan</span>
                <i class="fas fa-fw fa-chart-area"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/Beranda/')?>">
                <span >Detail Penggunaan Kendaraan</span>
                <i class="fas fa-fw fa-chart-area"></i>
              </a>
            </li> -->
            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
          </ul>

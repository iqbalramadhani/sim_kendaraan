         
       <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">
              <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                </div>
                <div class="sidebar-brand-text mx-3">Admin </div>
              </a>
                  <hr class="sidebar-divider my-0">
                  <li class="nav-item active">
                              <a class="nav-link" href="<?php echo base_url('index.php/Admin/')?>">
                                <span class="menu-title">Data Kendaraan</span>
                                <!-- <i class="mdi mdi-format-list-numbered menu-icon"></i> -->
                              </a>
                  </li>
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url('index.php/Admin/pengguna')?>">
                            <span >Data Pengguna</span>
                            <!-- <i class="nc-icon nc-single-02"></i> -->
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url('index.php/Admin/pegawai')?>">
                            <span >Data Pegawai</span>
                            <i class="fas fa-fw fa-chart-area"></i>
                          </a>
                        </li>

                        <div class="text-center d-none d-md-inline">
                          <button class="rounded-circle border-0" id="sidebarToggle"></button>
                        </div>
                      </ul>


<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Kepala Admin </div>
  </a>
      <hr class="sidebar-divider my-0">

      <li class="nav-item active">
         <a class="nav-link"  href="<?php echo base_url('index.php/Kaadm_logistik')?>">
           <span>Data Penggunaan Sparepart</span>
           <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
         </a>
       </li>
       <li class="nav-item" >
         <a class="nav-link" href="<?php echo base_url('index.php/Kaadm_logistik/monitor_ban')?>">
           <span>Data Penggunaan Ban</span>
           <!-- <i class="nc-icon nc-bell-55"></i> -->
         </a>
       </li>
       <li class="nav-item" >
         <a class="nav-link" href="<?php echo base_url('index.php/Beranda/')?>">
           <span>Data Penggunaan Kendaraan</span>
           <!-- <i class="nc-icon nc-bell-55"></i> -->
         </a>
       </li>


            <div class="sidebar-heading">
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item ">
              <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Rekomendasi</span>
              </a>
              <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header"></h6>
                  <a class="collapse-item" href="<?php echo base_url('index.php/Kaadm_Logistik/kriteria/')?>">Kriteria</a>
                  <a class="collapse-item" href="<?php echo base_url('index.php/Saw/')?>">Hitung Kriteria</a>
                </div>
              </div>
            </li>
            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
          </ul>

<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

  <!-- Sidebar Toggle (Topbar) -->
  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
  </button>

<ul class="navbar-nav ml-auto">
  <li class="nav-item dropdown no-arrow mx-1">
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="<?php echo base_url('/assets/img/logo-small.png')?>">
          </div>
        </a>
    <a class="nav-link dropdown-toggle" href="<?php echo site_url('Login/logout'); ?>"  role="button">
      <i class="fas fa-sign-out-alt"></i>
      LOGOUT
    </a>
    </ul>
  </nav>

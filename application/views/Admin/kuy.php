<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">



<br>
<br>
<br>
<br>
<br>
<br>

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Username</th>
            <th>Role</th>
            <th>Password</th>
            <th>Email</th>
            <th>ID Pegawai</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach ($haha as $st) : ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $st->username ?></td>
                <th><?= $st->role ?></th>
                <th><?= $st->password ?></th>
                <th><?= $st->id_pegawai ?></th>
                <td>
                    <div class="btn-group btn-group-sm">
                        <a onclick=deletedata(<?= $st->id_user; ?>) class="btn btn-info text-white"><span class="fa-check fa"> Setujui</a>
                    </div>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Username</th>
            <th>Role</th>
            <th>Password</th>
            <th>Email</th>
            <th>ID Pegawai</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
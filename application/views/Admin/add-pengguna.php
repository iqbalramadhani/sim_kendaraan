
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data pengguna</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Admin/pengguna'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>Id User<span class="text-danger">*</span></label>
            <input type="text" name="id_user" id="id_user" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Username<span class="text-danger">*</span></label>
            <input type="text" name="username" id="username" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Role  <span class="text-danger">*</span></label>
            <select name="role" class="form-control" id="">
            <option value="kaop_bus" selected>kaop_bus</option>
            <option value="kaop_truk">kaop_truk</option>
            <option value="Admin">Admin</option>
            <option value="operator">operator</option>
          </select>
          </div>
          <div class="form-group">
            <label>Password<span class="text-danger">*</span></label>
            <input type="password" name="password" id="password" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Email<span class="text-danger">*</span></label>
            <input type="text" name="email" id="email" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Id Pegawai<span class="text-danger">*</span></label>
            <select name="id_pegawai" class="form-control" id="">
            <?php
            if(!empty($DataPegawai)){
            foreach ($DataPegawai as $key) {
           if($key->id_pegawai == $data->id_pegawai){
            echo ' <option value="'.$key->id_pegawai.'" selected>'.$key->nama.'</option>';
          }else{
                echo ' <option value="'.$key->id_pegawai.'">'.$key->nama.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>




        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>

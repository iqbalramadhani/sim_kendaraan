
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Kendaraan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Admin/'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>id kendaraan <span class="text-danger">*</span></label>
            <input type="text" name="id_kendaraan" id="id_kendaraan" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Jenis <span class="text-danger">*</span></label>
            <select name="jenis" id="jenis" class="form-control">
              <option value="Bus">Bus</option>
              <option value="Truk">Dump Truk</option>
            </select>
          </div>
          <div class="form-group">
            <label>Nomor Polisi <span class="text-danger">*</span></label>
            <input type="text" name="nopol" id="nopol" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>merk <span class="text-danger">*</span></label>
            <input type="text" name="merk" id="merk" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Tahun <span class="text-danger">*</span></label>
            <input type="text" name="tahun" id="tahun" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <!-- <div class="form-group">
            <label>Ban <span class="text-danger">*</span></label>
            <select name="ban" id="ban" class="form-control">
              <option value="tipis">Ban Menipis</option>
              <option value="tebal">Ban Tebal</option>
            </select>
          </div>
          <div class="form-group">
            <label>Oli <span class="text-danger">*</span></label>
            <select name="oli" id="oli" class="form-control">
              <option value="setengah">Setengah</option>
              <option value="seperempat">Seperempat</option>
              <option value="full">Full</option>
            </select>
          </div><div class="form-group">
            <label>Air Radiator <span class="text-danger">*</span></label>
            <select name="air_radiator" id="air_radiator" class="form-control">
              <option value="setengah">Setengah</option>
              <option value="seperempat">Seperempat</option>
              <option value="full">Full</option>
            </select>
          </div>
          <div class="form-group">
            <label>AC <span class="text-danger">*</span></label>
            <select name="ac" id="ac" class="form-control">
              <option value="dingin">Dingin</option>
              <option value="tidak_dingin">Tidak Dingin</option>
            </select>
          </div> -->
        </div>


        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="modal" id="Edit<?php echo $data->id;?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Monitor_Rutin/'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>ID<span class="text-danger">*</span></label>
            <input type="text" name="id" id="id" value="<?php echo $data->id; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>Id Kendaraan <span class="text-danger">*</span></label>
            <select name="id_kendaraan" class="form-control" id="">
            <?php
            if(!empty($DataKendaraan)){
            foreach ($DataKendaraan as $key) {
           if($key->id_kendaraan == $data->id_kendaraan){
            echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->id_kendaraan.'</option>';
          }else{
                echo ' <option value="'.$key->id_kendaraan.'">'.$key->id_kendaraan.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>ban <span class="text-danger">*</span></label>
            <input type="text" name="ban" id="ban" value="<?php echo $data->ban; ?>" class="form-control" >
          </div>

          <div class="form-group">
            <label>oli<span class="text-danger">*</span></label>
            <input type="text" name="oli" id="oli" value="<?php echo $data->oli; ?>" class="form-control" >
          </div>

          <div class="form-group">
            <label>air_radiator<span class="text-danger">*</span></label>
            <input type="text" name="air_radiator" id="air_radiator" value="<?php echo $data->air_radiator; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>ac <span class="text-danger">*</span></label>
            <input type="text" name="ac" id="ac" value="<?php echo $data->ac; ?>" class="form-control" >
          </div>
          </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>
</div>
    </div>
  </div>


<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Admin/pegawai'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">
           <div class="form-group">
            <label>ID Pegawai<span class="text-danger">*</span></label>
            <input type="text" name="id_pegawai" id="id_pegawai" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Nama<span class="text-danger">*</span></label>
            <input type="text" name="nama" id="nama" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>


          <div class="form-group">
            <label>jabatan<span class="text-danger">*</span></label>
            <input type="text" name="jabatan" id="jabatan" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>




        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>

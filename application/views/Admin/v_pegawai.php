
<h1 class="h4 mb-2 text-gray-800">Data Pegawai</h1>
<div class="card shadow mb-4">
        <div class="card-body">
          <p align="right">

          <a href="#" data-toggle="modal" data-target="#Add" class="btn btn-danger">Tambah Pegawai </a>
          <?php include 'add-pegawai.php' ?>
          </p>

          <div class="table-responsive">
            <table class="table table-bordered" id="example" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Id Pegawai</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Jabatan</th>



                  <th scope="col">Aksi</th>
                </tr>
              </thead>

              <tbody>
                <?php
                        if( ! empty($DataPegawai)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                          $i = 1;
                          foreach($DataPegawai as $data){


                            echo
                            '<tr>
                            <td>'.$i++.'</td>
                            <td>'.$data->id_pegawai.'</td>
                              <td>'.$data->nama.'</td>
                              <td>'.$data->jabatan.'</td>

                              <td>
                              <a href="#" data-toggle="modal" data-target="#Edit'.$data->id_pegawai.'"
      class="btn btn-info">Edit
      <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  </a>

      <a href="#" data-toggle="modal" data-target="#Delete'.$data->id_pegawai.'" class="btn btn-danger">Delete </a>
      </td>
      </tr>';
      include 'edit-pegawai.php';
      include 'delete-pegawai.php';




                          }
                        }else{ // Jika data  kosong
                          echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                        }
                        ?>
                      </tbody>
                    </table>

                  </div>
                </div>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollX": true
        });
    });
</script>


<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
</script>


<form method="post" action="<?php echo base_url("index.php/Kaadm_logistik/form_ban"); ?>" enctype="multipart/form-data">

	<input type="file"  name="file">
	<br>

	<input type="submit" class="btn btn-primary" name="preview" value="Preview">
</form>

<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}
		
		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("index.php/Kaadm_logistik/import_ban")."'>";
		
		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";
		
		echo "
		<table class='table' id='example'> 
		<thead>
		<tr>
		<th>id maintain</th>
		<th>id kendaraan</th>
		<th>sopir</th>
		<th>No</th>
		<th>Nama Barang</th>
		<th>merk</th>
		<th>km_pakai</th>
		<th>harga</th>
		<th>tgl_lepas</th>
		</tr>";
		
		$numrow = 1;
		$kosong = 0;
		
		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($data['sheet'] as $row){ 
			// Ambil data pada excel sesuai Kolom
					$id_monitoring = $row['A']; // Insert data id_monitoring dari kolom A di excel
                    $id_kendaraan=$row['B']; // Insert data nama dari kolom B di excel
                    $sopir=$row['C']; //Insert data sopir booking dari kolom B di excel
                    $tgl_pakai=$row['D']; // Insert data tujuan dari kolom C di excel
                    $nomor_ban=$row['E']; // Insert data jemput dari kolom C di excel
                    $merk=$row['F']; // Insert data jam dari kolom C di excel
                    $km_pakai=$row['G']; // Insert data tgl pergi dari kolom C di excel
                    $harga=$row['H']; // Insert data tgl pulang dari kolom C di excel
                    $tgl_lepas=$row['I']; // Insert data bus dari kolom C di excel   
                    $km_lepas=$row['J'];
                    $balance=$row['K'];
                    $harga_klaim=$row['L'];
                    $shor_over=$row['M'];
                    $km=$row['N'];
                    $shor_over2=$row['O'];
                    $ban_bekas=$row['P'];
			
			
			// Cek jika semua data tidak diisi
			if(empty($id_monitoring) && empty($id_kendaraan) && empty($sopir) && empty($tgl_pakai) 
				&& empty($nomor_ban) && empty($merk) && empty($km_pakai) && empty($harga) 
				&& empty($tgl_lepas) && empty($km_lepas) && empty($balance) && empty($harga_klaim) && empty($shor_over) && empty($km) && empty($shor_over2) && empty($ban_bekas))
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
			
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah id_kendaraan-id_kendaraan kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$id_monitoring_td = ( ! empty($id_monitoring))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$id_kendaraan_td = ( ! empty($id_kendaraan))? "" : " style='background: #E07171;'"; // Jika Nama 

				$sopir_td = ( ! empty($sopir))? "" : " style='background: #E07171;'"; // 

				$tgl_pakai_td = ( ! empty($tgl_pakai))? "" : " style='background: #E07171;'";

				$nomor_ban_td = ( ! empty($nomor_ban))? "" : " style='background: #E07171;'";

				$merk_td = ( ! empty($merk))? "" : " style='background: #E07171;'";

				$km_pakai_td = ( ! empty($km_pakai))? "" : " style='background: #E07171;'";
				
				$harga_td = ( ! empty($harga))? "" : " style='background: #E07171;'";
				
				$tgl_lepas_td = ( ! empty($tgl_lepas))? "" : " style='background: #E07171;'";
				// Jika salah satu data ada yang kosong
				$km_lepas_td = ( ! empty($km_lepas))? "" : " style='background: #E07171;'";
				$balance_td = ( ! empty($balance))? "" : " style='background: #E07171;'";
				$harga_klaim_td = ( ! empty($harga_klaim))? "" : " style='background: #E07171;'";
				$shor_over_td = ( ! empty($shor_over))? "" : " style='background: #E07171;'";
				$km_td = ( ! empty($km))? "" : " style='background: #E07171;'";
				$shor_over2_td = ( ! empty($shor_over2))? "" : " style='background: #E07171;'";
				$ban_bekas_td = ( ! empty($ban_bekas))? "" : " style='background: #E07171;'";

				
				if(empty($id_monitoring) or empty($id_kendaraan) or empty($sopir) or empty($tgl_pakai) 
					or empty($nomor_ban) or empty($merk) or empty($km_pakai) or empty($harga) or empty($tgl_lepas) or empty($km_lepas) or empty($balance) or empty($harga_klaim) or empty($shor_over) or empty($km) or empty($shor_over2) or empty($ban_bekas)) {
					$kosong++; // Tambah 1 variabel $kosong
			}


			echo "<tr>";
			echo "<td".$id_monitoring_td.">".$id_monitoring."</td>";
			echo "<td".$id_kendaraan_td.">".$id_kendaraan."</td>";
			echo "<td".$sopir_td.">".$sopir."</td>";
			echo "<td".$tgl_pakai_td.">".$tgl_pakai."</td>";
			echo "<td".$nomor_ban_td.">".$nomor_ban."</td>";
			echo "<td".$merk_td.">".$merk."</td>";
			echo "<td".$km_pakai_td.">".$km_pakai."</td>";
			echo "<td".$harga_td.">".$harga."</td>";
			echo "<td".$tgl_lepas_td.">".$tgl_lepas."</td>";
			echo "<td".$km_lepas_td.">".$km_lepas."</td>";
			echo "<td".$balance_td.">".$balance."</td>";
			echo "<td".$harga_klaim_td.">".$harga_klaim."</td>";
			echo "<td".$shor_over_td.">".$shor_over."</td>";
			echo "<td".$km_td.">".$km."</td>";
			echo "<td".$shor_over2_td.">".$shor_over2."</td>";
			echo "<td".$ban_bekas_td.">".$ban_bekas."</td>";
			echo "</tr>";
		}

			$numrow++; // Tambah 1 setiap kali looping
		}
		echo "</thead>";
		
		echo "</table>";
		
		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
			?>	
			<script>
				$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');
				
				$("#kosong").show(); // Munculkan alert validasi kosong
			});
		</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";
			
			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' class='btn btn-rounded-primary' name='import'>Import</button>";
			echo "<a href='".base_url("index.php/Kaadm_logistik/monitor_ban")."' class='btn btn-danger'>Cancel</a>";
		}
		
		echo "</form>";
	}
	?>



	<script>
		$(document).ready( function () {
			$('#example').DataTable();
		} );

	</script>



<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
</script>


<form method="post" action="<?php echo base_url("index.php/Kaadm_logistik/form"); ?>" enctype="multipart/form-data">

	<input type="file"  name="file">
	<br>

	<input type="submit" class="btn btn-primary" name="preview" value="Preview">
</form>

<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}
		
		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("index.php/Kaadm_logistik/import")."'>";
		
		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";
		
		echo "
		<table class='table' id='example'> 
		<thead>
		<tr>
		<th>id maintain</th>
		<th>id kendaraan</th>
		<th>Tanggal</th>
		<th>No</th>
		<th>Nama Barang</th>
		<th>Deskripsi</th>
		<th>Quantity</th>
		<th>Unit</th>
		<th>Total</th>
		</tr>";
		
		$numrow = 1;
		$kosong = 0;
		
		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($data['sheet'] as $row){ 
			// Ambil data pada excel sesuai Kolom
			$id_maintain = $row['A']; // Ambil data NIS
			$id_kendaraan = $row['B']; // Ambil data id_kendaraan
			$tanggal = $row['C'];
			$nomor = $row['D'];
			$nama_barang = $row['E'];
			$deskripsi = $row['F'];
			$quantity = $row['G'];
			$unit = $row['H'];
			$total =$row['I'];
			
			// Cek jika semua data tidak diisi
			if(empty($id_maintain) && empty($id_kendaraan) && empty($tanggal) && empty($nomor) 
				&& empty($nama_barang) && empty($deskripsi) && empty($quantity) && empty($unit) 
				&& empty($total))
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
			
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah id_kendaraan-id_kendaraan kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$id_maintain_td = ( ! empty($id_maintain))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$id_kendaraan_td = ( ! empty($id_kendaraan))? "" : " style='background: #E07171;'"; // Jika Nama 

				$tanggal_td = ( ! empty($tanggal))? "" : " style='background: #E07171;'"; // 

				$nomor_td = ( ! empty($nomor))? "" : " style='background: #E07171;'";

				$nama_barang_td = ( ! empty($nama_barang))? "" : " style='background: #E07171;'";

				 $deskripsi_td = ( ! empty($deskripsi))? "" : " style='background: #E07171;'";

				$quantity_td = ( ! empty($quantity))? "" : " style='background: #E07171;'";
				
				$unit_td = ( ! empty($unit))? "" : " style='background: #E07171;'";
				
				$total_td = ( ! empty($total))? "" : " style='background: #E07171;'";
				// Jika salah satu data ada yang kosong
				
				if(empty($id_maintain) or empty($id_kendaraan) or empty($tanggal) or empty($nomor) 
					or empty($nama_barang) or empty($deskripsi) or empty($quantity) or empty($unit) or empty($total) ){
					$kosong++; // Tambah 1 variabel $kosong
				}
				
				
				echo "<tr>";
				echo "<td".$id_maintain_td.">".$id_maintain."</td>";
				echo "<td".$id_kendaraan_td.">".$id_kendaraan."</td>";
				echo "<td".$tanggal_td.">".$tanggal."</td>";
				echo "<td".$nomor_td.">".$nomor."</td>";
				echo "<td".$nama_barang_td.">".$nama_barang."</td>";
				echo "<td".$deskripsi_td.">".$deskripsi."</td>";
				echo "<td".$quantity_td.">".$quantity."</td>";
				echo "<td".$unit_td.">".$unit."</td>";
				echo "<td".$total_td.">".$total."</td>";
				echo "</tr>";
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}
		echo "</thead>";
		
		echo "</table>";
		
		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
			?>	
			<script>
				$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');
				
				$("#kosong").show(); // Munculkan alert validasi kosong
			});
		</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";
			
			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' class='btn btn-primary' name='import'>Import</button>";
			echo "<a href='".base_url("index.php/Kaadm_logistik")."' class='btn btn-danger'>Cancel</a>";
		}
		
		echo "</form>";
	}
	?>



	<script>
		$(document).ready( function () {
			$('#example').DataTable();
		} );

	</script>


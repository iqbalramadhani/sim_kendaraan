
<!-- The Modal -->
<div class="modal" id="Edit<?php echo $data->kode_booking;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaop_bus'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">

          <div class="form-group">
            <label>kode booking <span class="text-danger">*</span></label>
            <input type="text" name="kode_booking" id="kode_booking" value="<?php echo $data->kode_booking; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Nama <span class="text-danger">*</span></label>
            <input type="text" name="nama" id="nama" value="<?php echo $data->nama;?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Tanggal Booking<span class="text-danger">*</span></label>
            <input type="date" name="tgl_booking" id="tgl_booking" value="<?php echo $data->tgl_booking; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>lama<span class="text-danger">*</span></label>
            <input type="text" name="lama" id="lama" value="<?php echo $data->lama; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>tujuan<span class="text-danger">*</span></label>
            <input type="text" name="tujuan" id="tujuan" value="<?php echo $data->tujuan; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Jemput<span class="text-danger">*</span></label>
            <input type="text" name="jemput" id="jemput" value="<?php echo $data->jemput; ?>" class="form-control">
          </div>

          <div class="form-group">
            <label>jam <span class="text-danger">*</span></label>
            <input type="time" name="jam" id="jam" value="<?php echo $data->jam; ?>" class="form-control">
          </div>
          <input type="hidden" value="<?php echo $data->kode_booking; ?>" name="kode_booking">
        </div>
        <div class="form-group">
          <label>Tanggal Berangkat<span class="text-danger">*</span></label>
          <input type="date" name="tgl_berangkat" id="tgl_berangkat" value="<?php echo $data->tgl_berangkat; ?>" class="form-control">
        </div>
        <div class="form-group">
          <label>Tanggal Pulang<span class="text-danger">*</span></label>
          <input type="date" name="tgl_pulang" id="tgl_pulang" value="<?php echo $data->tgl_pulang; ?>" class="form-control">
        </div>

        <div class="form-group">
          <label>id_kendaraan<span class="text-danger">*</span></label>
          <input type="text" name="id_kendaraan" id="id_kendaraan" value="<?php echo $data->id_kendaraan; ?>" class="form-control">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>
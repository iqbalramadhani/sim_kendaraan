
<!-- The Modal -->
<div class="modal" id="Edit<?php echo $data->id_kendaraan;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaop_bus/kendaraan'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">

          <div class="form-group">
            <label>id_kendaraan <span class="text-danger">*</span></label>
            <input type="text" name="id_kendaraan" id="id_kendaraan" value="<?php echo $data->id_kendaraan; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>jenis <span class="text-danger">*</span></label>
            <select name="jenis" id="jenis"  value="<?php echo $data->jenis; ?>" class="form-control">
              <option value="Bus">Bus</option>
              <option value="Truk">Dump Truk</option>
            </select>
          </div>
          <div class="form-group">
            <label>No Polisi <span class="text-danger">*</span></label>
            <input type="text" name="nopol" id="nopol" value="<?php echo $data->nopol; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>merk<span class="text-danger">*</span></label>
            <input type="text" name="merk" id="merk" value="<?php echo $data->merk; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>tahun<span class="text-danger">*</span></label>
            <input type="text" name="tahun" id="tahun" value="<?php echo $data->tahun; ?>" class="form-control">
          </div>
           <div class="form-group">
            <label>status <span class="text-danger">*</span></label>
            <select name="status" id="jenis"  value="<?php echo $data->status; ?>" class="form-control">
              <option value="avaible">Tersedia</option>
              <option value="perbaikan">Perbaikan</option>
              <option value="booked">Booked</option>
            </select>
          </div>


          <input type="hidden" value="<?php echo $data->id_kendaraan; ?>" name="id_kendaraan">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>
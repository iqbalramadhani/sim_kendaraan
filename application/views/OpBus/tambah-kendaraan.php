
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Kendaraan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaop_bus/kendaraan'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">

        
          <div class="form-group">
            <label>id kendaraan <span class="text-danger">*</span></label>
            <input type="text" name="id_kendaraan" id="id_kendaraan" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>Jenis <span class="text-danger">*</span></label>
            <select name="jenis" id="jenis" class="form-control">
              <option value="Bus">Bus</option>
            </select>
          </div>
          <div class="form-group">
            <label>Nomor Polisi <span class="text-danger">*</span></label>
            <input type="text" name="nopol" id="nopol" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>merk <span class="text-danger">*</span></label>
            <input type="text" name="merk" id="merk" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>Tahun <span class="text-danger">*</span></label>
            <input type="text" name="tahun" id="tahun" class="form-control" required="">
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>
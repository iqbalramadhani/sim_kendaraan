
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Sewa</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Kaop_bus'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">

        
          <div class="form-group">
            <label>kode_booking <span class="text-danger">*</span></label>
            <input type="text" name="kode_booking" id="kode_booking" class="form-control" required="">
            <!-- <input type="text" name="kode_booking" id="kode_booking" class="form-control" value="<?= $kodeunik; ?>" readonly> -->
          </div>
          <div class="form-group">
            <label>Nama <span class="text-danger">*</span></label>
            <input type="text" name="nama" id="nama" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>Tanggal Booking <span class="text-danger">*</span></label>
            <input type="text" name="tgl_booking" id="tgl_booking" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>tujuan <span class="text-danger">*</span></label>
            <input type="text" name="tujuan" id="tujuan" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>jemput<span class="text-danger">*</span></label>
            <input type="text" name="jemput" id="jemput" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>jam<span class="text-danger">*</span></label>
            <input type="time" name="jam" id="jam" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>tgl_berangkat <span class="text-danger">*</span></label>
            <input type="date" name="tgl_berangkat" id="tgl_berangkat" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>tgl_pulang <span class="text-danger">*</span></label>
            <input type="date" name="tgl_pulang" id="tgl_pulang" class="form-control" required="">
          </div>
          
        <div class="form-group">
            <label>Bus <span class="text-danger">*</span></label>
            <select name="id_kendaraan" class="form-control" id="">          
            <?php 
            if(!empty($DataBus)){
            foreach ($DataBus as $key) {
           if($key->id_kendaraan == $data->id_kendaraan){
            echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->nopol.'</option>';
          }else{
                echo ' <option value="'.$key->id_kendaraan.'">'.$key->nopol.'</option>';
                }
              }
            }
             ?>
            </select>
          </div> 
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>
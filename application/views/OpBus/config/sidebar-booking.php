<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3"> Ka Op Bus Pariwisata</div>
  </a>
      <hr class="sidebar-divider my-0">


            <li class="nav-item active">
              <a class="nav-link" href="<?php echo base_url('index.php/Kaop_bus')?>">
                <span class="menu-title">Data Booking</span>
                <i class="mdi mdi-format-list-numbered menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/Kaop_bus/kendaraan/')?>">
                <span class="menu-title">Data Bus</span>
                <i class="mdi mdi-account-key menu-icon"></i>
              </a>
            </li>
            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
          </ul>

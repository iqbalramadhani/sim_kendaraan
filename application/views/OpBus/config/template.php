<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SIM Penggunaan Kendaraan</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/mdi/css/materialdesignicons.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.base.css')?>">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png')?>" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="<?php echo base_url('css/jquery.dataTables.min.css'); ?>" />
   
  <script src="<?php echo base_url('assets/js/core/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/core/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js')?>"></script>


  <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
   <script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
  </head>
  <body>
    <div class="container-scroller">
      <!-- navbar -->
       <?php include_once "config/navbar.php"; ?>
    
      <!-- Sidebar -->
      
      <div class="container-fluid page-body-wrapper">
        <?php include_once $this->data['sidebar']; ?>
      <div class="main-panel">
     <?php $this->load->view($this->data['MainView']); ?>
     
      <div class="content-wrapper">
      
      </div>
      <?php include_once "config/footer.php"; ?>
    </div>
  </div>
</div>
<script src="<?php echo base_url('assets/vendors/js/vendor.bundle.base.js')?>"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<?php echo base_url('assets/js/off-canvas.js')?>"></script>
    <script src="<?php echo base_url('assets/js/hoverable-collapse.js')?>"></script>
    <script src="<?php echo base_url('assets/js/misc.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/perfect-scrollbar.jquery.min.js')?>"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartsPages();
    });
  </script>
    <!-- endinject -->
  </body>
</html>
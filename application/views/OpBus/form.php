
<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
</script>

<a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
<br>
<br>


<form method="post" action="<?php echo base_url("index.php/Kaop_Bus/form"); ?>" enctype="multipart/form-data">

	<input type="file"  name="file">
	<br>

	<input type="submit" class="btn btn-primary" name="preview" value="Preview">
</form>

<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("index.php/Kaop_Bus/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "
		<table class='table' id='example'>
		<thead>
		<tr>
		<th>kode_booking</th>
		<th>Nama</th>
		<th>Tanggal Booking</th>
		<th>Tujuan </th>
		<th>Jemput </th>
		<th>Jam</th>
		<th>Tanggal Berangkat</th>
		<th>Tanggal Pulang</th>
		<th>Bus</th>
		</tr>";

		$numrow = 1;
		$kosong = 0;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($data['sheet'] as $row){
			// Ambil data pada excel sesuai Kolom
			$kode_booking = $row['A']; // Ambil data NIS
			$periode = $row['B'];
			$nama = $row['C']; // Ambil data nama
			$tgl_booking = $row['D'];
			$tujuan = $row['E'];
			$jemput = $row['F'];
			$jam = $row['G'];
			$tgl_berangkat = $row['H'];
			$tgl_pulang = $row['I'];
			$bus =$row['J'];

			// Cek jika semua data tidak diisi
			if(empty($kode_booking) && empty($periode) && empty($nama) && empty($tgl_booking) && empty($tujuan)
				&& empty($jemput) && empty($jam) && empty($tgl_berangkat) && empty($tgl_pulang)
				&& empty($bus))
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$kode_booking_td = ( ! empty($kode_booking))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah

				$periode_td = ( ! empty($periode))? "" : " style='background: #E07171;'"; //

				$nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'"; // Jika Nama

				$tgl_booking_td = ( ! empty($tgl_booking))? "" : " style='background: #E07171;'"; //

				$tujuan_td = ( ! empty($tujuan))? "" : " style='background: #E07171;'";

				$jemput_td = ( ! empty($jemput))? "" : " style='background: #E07171;'";

				 $jam_td = ( ! empty($jam))? "" : " style='background: #E07171;'";

				$tgl_berangkat_td = ( ! empty($tgl_berangkat))? "" : " style='background: #E07171;'";

				$tgl_pulang_td = ( ! empty($tgl_pulang))? "" : " style='background: #E07171;'";

				$bus_td = ( ! empty($bus))? "" : " style='background: #E07171;'";
				// Jika salah satu data ada yang kosong

				if(empty($kode_booking) or empty($periode) or empty($nama) or empty($tgl_booking) or empty($tujuan)
					or empty($jemput) or empty($jam) or empty($tgl_berangkat) or empty($tgl_pulang) or empty($bus) ){
					$kosong++; // Tambah 1 variabel $kosong
				}


				echo "<tr>";
				echo "<td".$kode_booking_td.">".$kode_booking."</td>";
				echo "<td".$periode_td.">".$periode."</td>";
				echo "<td".$nama_td.">".$nama."</td>";
				echo "<td".$tgl_booking_td.">".$tgl_booking."</td>";
				echo "<td".$tujuan_td.">".$tujuan."</td>";
				echo "<td".$jemput_td.">".$jemput."</td>";
				echo "<td".$jam_td.">".$jam."</td>";
				echo "<td".$tgl_berangkat_td.">".$tgl_berangkat."</td>";
				echo "<td".$tgl_pulang_td.">".$tgl_pulang."</td>";
				echo "<td".$bus_td.">".$bus."</td>";
				echo "</tr>";
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		echo "</thead>";

		echo "</table>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
			?>
			<script>
				$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

				$("#kosong").show(); // Munculkan alert validasi kosong
			});
		</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' class='btn btn-primary' name='import'>Import</button>";
			echo "<a href='".base_url("index.php/Booking")."' class='btn btn-danger'>Cancel</a>";
		}

		echo "</form>";
	}
	?>



	<script>
		$(document).ready( function () {
			$('#example').DataTable();
		} );

	</script>

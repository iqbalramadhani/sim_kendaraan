<div class="modal" id="Edit<?php echo $data->id_penggunaan_ban;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Staff_Mekanik/monitor_ban'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>Id Monitoring<span class="text-danger">*</span></label>
            <input type="text" name="id_penggunaan_ban" id="id_penggunaan_ban" value="<?php echo $data->id_penggunaan_ban; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>Id Kendaraan <span class="text-danger">*</span></label>
            <select name="id_kendaraan" value="<?php echo $data->id_kendaraan; ?>" class="form-control" >
            <?php
            if(!empty($DataKendaraan)){
            foreach ($DataKendaraan as $key) {
           if($key->id_kendaraan == $data->id_kendaraan){
            echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->id_kendaraan.'</option>';
          }else{
                echo ' <option value="'.$key->id_kendaraan.'">'.$key->id_kendaraan.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>Id Periode<span class="text-danger">*</span></label>
            <select name="id_periode" id="id_periode" value="<?php echo $data->id_kendaraan; ?>" class="form-control">
              <option value="719">Juli 2019</option>
              <option value="819">Agustus 2019</option>
              <option value="919">September 2019</option>
              <option value="1019">Oktober 2019</option>
              <option value="1119">November 2019</option>
              <option value="1219">Desember 2019</option>
            </select>
          </div>
          <div class="form-group">
            <label>id_sopir<span class="text-danger">*</span></label>
            <input type="text" name="id_sopir" id="id_sopir" value="<?php echo $data->id_sopir; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>Tanggal Pakai <span class="text-danger">*</span></label>
            <input type="date" name="tgl_pakai" id="tgl_pakai" value="<?php echo $data->tgl_pakai; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>ID Ban <span class="text-danger">*</span></label>
            <input type="text" name="id_ban" id="id_ban" value="<?php echo $data->id_ban; ?>" class="form-control" >
          </div>


          <div class="form-group">
            <label>KM Pakai <span class="text-danger">*</span></label>
            <input type="text" name="km_pakai" id="km_pakai" value="<?php echo $data->km_pakai; ?>" class="form-control" >
          </div>

          <div class="form-group">
            <label>Tanggal Lepas <span class="text-danger">*</span></label>
            <input type="date" name="tgl_lepas" id="tgl_lepas" value="<?php echo $data->tgl_lepas; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>KM Lepas <span class="text-danger">*</span></label>
            <input type="text" name="km_lepas" id="km_lepas" value="<?php echo $data->km_lepas; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>Balance <span class="text-danger">*</span></label>
            <input type="text" name="balance" id="balance" value="<?php echo $data->balance; ?>" class="form-control" >
          </div>

          <div class="form-group">
            <label>shor over <span class="text-danger">*</span></label>
            <input type="text" name="shor_over" id="shor_over" value="<?php echo $data->shor_over; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>KM <span class="text-danger">*</span></label>
            <input type="text" name="km" id="km" value="<?php echo $data->km; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>shor over <span class="text-danger">*</span></label>
            <input type="text" name="shor_over2" id="shor_over2" value="<?php echo $data->shor_over2; ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>ban bekas <span class="text-danger">*</span></label>
            <input type="text" name="ban_bekas" id="ban_bekas" value="<?php echo $data->ban_bekas; ?>" class="form-control" >
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>

<h1 class="h4 mb-2 text-gray-800">Data Penggunaan Ban</h1>
<div class="card shadow mb-4">
        <div class="card-body">
          <p align="right">
          <a href="#" data-toggle="modal" data-target="#Add" class="btn btn-danger">Tambah </a>
          <?php include 'add-monitoring-ban.php';?>
          </p>

          <div class="table-responsive">
            <table class="table table-bordered" id="example" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Id Penggunaan Ban</th>
                  <th scope="col">Id Kendaraan</th>
                  <th scope="col">Id Periode</th>
                  <th scope="col">Id Sopir</th>
                  <th scope="col">Tanggal Pakai</th>
                  <th scope="col">ID Ban</th>
                  <th scope="col">KM Pakai</th>
                  <th scope="col">Tgl Lepas</th>
                  <th scope="col">KM Lepas</th>
                  <th scope="col">Balance</th>
                  <th scope="col">Shor Over</th>
                  <th scope="col">KM</th>
                  <th scope="col">Shor Over 2</th>
                  <th scope="col">Ban Bekas</th>


                  <th scope="col">Aksi</th>
                </tr>
              </thead>

              <tbody>
                <?php
                        if( ! empty($DataMonitorBan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                          $i = 1;
                          foreach($DataMonitorBan as $data){

                            echo
                            '<tr>
                            <td>'.$i++.'</td>
                              <td>'.$data->id_penggunaan_ban.'</td>
                              <td>'.$data->id_kendaraan.'</td>
                              <td>'.$data->id_periode.'</td>
                              <td>'.$data->id_sopir.'</td>
                              <td>'.$data->tgl_pakai.'</td>
                              <td>'.$data->id_ban.'</td>
                              <td>'.$data->km_pakai.'</td>
                              <td>'.$data->tgl_lepas.'</td>
                              <td>'.$data->km_lepas.'</td>
                              <td>'.$data->balance.'</td>
                              <td>'.$data->shor_over.'</td>
                              <td>'.$data->km.'</td>
                              <td>'.$data->shor_over2.'</td>
                              <td>'.$data->ban_bekas.'</td>
                              <td>
                              <a href="#" data-toggle="modal" data-target="#Edit'.$data->id_penggunaan_ban.'"
      class="btn btn-info">Edit
      <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  </a>

      <a href="#" data-toggle="modal" data-target="#Delete'.$data->id_penggunaan_ban.'" class="btn btn-danger">Delete </a>
      </td>
      </tr>';
       include 'edit-monitoring-ban.php';
      include 'delete-monitoring-ban.php';




                          }
                        }else{ // Jika data  kosong
                          echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                        }
                        ?>
                      </tbody>
                    </table>

                  </div>
                </div>
            <script>
                    $(document).ready(function() {
                        $('#example').DataTable({
                            "scrollX": true
                        });
                    });
                </script>

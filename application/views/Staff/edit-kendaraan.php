
<!-- The Modal -->
<div class="modal" id="Edit<?php echo $data->id_kendaraan;  ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Data </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Staff_Mekanik/kendaraan'); ?>" method="POST" enctype="multipart/form-data">
        <!-- Modal body -->
        <div class="modal-body">

          <div class="form-group">
            <label>id_kendaraan <span class="text-danger">*</span></label>
            <input type="text" name="id_kendaraan" id="id_kendaraan" value="<?php echo $data->id_kendaraan; ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>jenis <span class="text-danger">*</span></label>
            <select name="jenis" id="jenis"  value="<?php echo $data->jenis; ?>" class="form-control">
              <option value="Bus">Bus</option>
              <option value="Truk">Dump Truk</option>
            </select>
          </div>
          <div class="form-group">
            <label>No Polisi <span class="text-danger">*</span></label>
            <input type="text" name="nopol" id="nopol" value="<?php echo $data->nopol; ?>" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>merk<span class="text-danger">*</span></label>
            <input type="text" name="merk" id="merk" value="<?php echo $data->merk; ?>" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>tahun<span class="text-danger">*</span></label>
            <input type="text" name="tahun" id="tahun" value="<?php echo $data->tahun; ?>" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>ban <span class="text-danger">*</span></label>
          <select name="ban" id="ban"  value="<?php echo $data->ban; ?>" class="form-control">
            <option value="tipis">Ban Menipis</option>
            <option value="tebal">Ban Tebal</option>
          </select>
          </div>

          <div class="form-group">
            <label>oli<span class="text-danger">*</span></label>
            <select name="oli" id="oli" value="<?php echo $data->oli; ?>"  class="form-control">
              <option value="setengah">Setengah</option>
              <option value="seperempat">Seperempat</option>
              <option value="full">Full</option>
            </select>
          </div>

          <div class="form-group">
            <label>air_radiator<span class="text-danger">*</span></label>
            <select name="air_radiator" id="air_radiator" value="<?php echo $data->air_radiator; ?>" class="form-control" >
              <option value="setengah">Setengah</option>
              <option value="seperempat">Seperempat</option>
              <option value="full">Full</option>
            </select>
          </div>
          <div class="form-group">
            <label>ac <span class="text-danger">*</span></label>
            <select name="ac" id="ac" value="<?php echo $data->ac; ?>" class="form-control" >
              <option value="dingin">Dingin</option>
              <option value="tidak_dingin">Tidak Dingin</option>
            </select>
          </div>



          <input type="hidden" value="<?php echo $data->id_kendaraan; ?>" name="id_kendaraan">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="edit" class="btn btn-danger"  name="edit">Edit</button>
        </div>
      </form>

    </div>
  </div>
</div>

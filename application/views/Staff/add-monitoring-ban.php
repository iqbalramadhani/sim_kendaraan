
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Monitoring</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Staff_Mekanik/monitor_ban'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">

        <div class="form-group">
            <label>Id Penggunaan Ban<span class="text-danger">*</span></label>
            <input type="text" name="id_penggunaan_ban" id="id_penggunaan_ban" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Id Kendaraan <span class="text-danger">*</span></label>
            <select name="id_kendaraan" class="form-control" id="">
            <?php
            if(!empty($DataKendaraan)){
            foreach ($DataKendaraan as $key) {
           if($key->id_kendaraan == $data->id_kendaraan){
            echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->id_kendaraan.'</option>';
          }else{
                echo ' <option value="'.$key->id_kendaraan.'">'.$key->id_kendaraan.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>Id Sopir <span class="text-danger">*</span></label>
            <select name="id_sopir" class="form-control" id="">
            <?php
            if(!empty($DataSopir)){
            foreach ($DataSopir as $key) {
           if($key->id_sopir == $data->id_sopir){
            echo ' <option value="'.$key->id_sopir.'" selected>'.$key->id_sopir.'</option>';
          }else{
                echo ' <option value="'.$key->id_sopir.'">'.$key->id_sopir.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>Id Periode<span class="text-danger">*</span></label>
            <select name="id_periode" id="id_periode" class="form-control">
              <option value="719">Juli 2019</option>
              <option value="819">Agustus 2019</option>
              <option value="919">September 2019</option>
              <option value="1019">Oktober 2019</option>
              <option value="1119">November 2019</option>
              <option value="1219">Desember 2019</option>
            </select>
          </div>

          <div class="form-group">
            <label>Tanggal Pakai <span class="text-danger">*</span></label>
            <input type="date" name="tgl_pakai" id="tgl_pakai" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Id Ban <span class="text-danger">*</span></label>
            <input type="text" name="id_ban" id="id_ban" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>


          <div class="form-group">
            <label>KM Pakai <span class="text-danger">*</span></label>
            <input type="text" name="km_pakai" id="km_pakai" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>

          <div class="form-group">
            <label>Tanggal Lepas <span class="text-danger">*</span></label>
            <input type="date" name="tgl_lepas" id="tgl_lepas" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>KM Lepas <span class="text-danger">*</span></label>
            <input type="text" name="km_lepas" id="km_lepas" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Balance <span class="text-danger">*</span></label>
            <input type="text" name="balance" id="balance" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>

          <div class="form-group">
            <label>shor_over <span class="text-danger">*</span></label>
            <input type="text" name="shor_over" id="shor_over" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>KM <span class="text-danger">*</span></label>
            <input type="text" name="km" id="km" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>shor over <span class="text-danger">*</span></label>
            <input type="text" name="shor_over2" id="shor_over2" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>ban bekas <span class="text-danger">*</span></label>
            <input type="text" name="ban_bekas" id="ban_bekas" class="form-control" oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>

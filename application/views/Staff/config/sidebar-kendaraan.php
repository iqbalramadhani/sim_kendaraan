
<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">

    </div>
    <div class="sidebar-brand-text mx-3">Staff Mekanik </div>
  </a>
      <hr class="sidebar-divider my-0">

      <li class="nav-item">
         <a class="nav-link"  href="<?php echo base_url('index.php/Staff_Mekanik')?>">
           <span>Data Penggunaan Sparepart</span>
           <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
         </a>
       </li>
       <li class="nav-item" >
         <a class="nav-link" href="<?php echo base_url('index.php/Staff_Mekanik/monitor_ban')?>">
           <span>Data Penggunaan Ban</span>
           <!-- <i class="nc-icon nc-bell-55"></i> -->
         </a>
       </li>
       <li class="nav-item active" >
         <a class="nav-link" href="<?php echo base_url('index.php/Staff_Mekanik/kendaraan/')?>">
           <span>Data Kendaraan</span>
           <!-- <i class="nc-icon nc-bell-55"></i> -->
         </a>
       </li>


            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
          </ul>

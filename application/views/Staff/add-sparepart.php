
<!-- The Modal -->
<div class="modal" id="Add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Penggunaan Sparepart</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?php echo base_url('index.php/Staff_Mekanik/'); ?>" method="POST" enctype="multipart/form-data">

        <!-- Modal body -->
        <div class="modal-body">

        <div class="form-group">
            <label>Id maintain<span class="text-danger">*</span></label>
            <input type="text" name="id_maintain" id="id_maintain" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Nomor Polisi<span class="text-danger">*</span></label>
            <select name="nopol" class="form-control" id="">
            <?php
            if(!empty($DataKendaraan)){
            foreach ($DataKendaraan as $key) {
           if($key->id_kendaraan == $data->id_kendaraan){
            echo ' <option value="'.$key->id_kendaraan.'" selected>'.$key->nopol.'</option>';
          }else{
                echo ' <option value="'.$key->id_kendaraan.'">'.$key->nopol.'</option>';
                }
              }
            }
             ?>
            </select>
          </div>
          <div class="form-group">
            <label>Id Periode<span class="text-danger">*</span></label>
            <select name="id_periode" id="id_periode" class="form-control">
              <option value="719">Juli 2019</option>
              <option value="819">Agustus 2019</option>
              <option value="919">September 2019</option>
              <option value="1019">Oktober 2019</option>
              <option value="1119">November 2019</option>
              <option value="1219">Desember 2019</option>
            </select>
          </div>

          <div class="form-group">
            <label>Tanggal <span class="text-danger">*</span></label>
            <input type="date" name="tanggal" id="tanggal" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Nomor <span class="text-danger">*</span></label>
            <input type="number" name="nomor" id="nomor" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Nama Barang<span class="text-danger">*</span></label>
            <input type="text" name="nama_barang" id="nama_barang" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>
          <div class="form-group">
            <label>Deskripsi <span class="text-danger">*</span></label>
            <input type="text" name="deskripsi" id="deskripsi" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>

          <div class="form-group">
            <label>quantity <span class="text-danger">*</span></label>
            <input type="text" name="quantity" id="quantity" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>

          <div class="form-group">
            <label>Unit<span class="text-danger">*</span></label>

            <select name="unit" class="form-control" id="">
              <option id="set">pcs</option>
              <option id="set">set</option>
              <option id="mtr">mtr</option></select>
          </div>
          <div class="form-group">
            <label>Total <span class="text-danger">*</span></label>
            <input type="text" name="total" id="total" class="form-control" required oninvalid="this.setCustomValidity('kolom tidak boleh kosong')" oninput="setCustomValidity('')">
          </div>

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="submit" value="add" class="btn btn-danger"  name="add">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>

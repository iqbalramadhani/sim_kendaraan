<h1 class="h4 mb-2 text-gray-800">Data kendaraan</h1>
<div class="card shadow mb-4">
        <div class="card-body">

          <div class="table-responsive">
            <table class="table table-bordered" id="example" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">ID Kendaraan</th>
                  <th scope="col">Jenis</th>
                  <th scope="col"> Nomor Polisi</th>
                  <th scope="col">Merk</th>
                  <th scope="col">Tahun</th>
                  <th scope="col"> Ban</th>
                  <th scope="col"> Oli</th>
                  <th scope="col"> Air Radiator</th>
                  <th scope="col"> AC</th>

                  <th scope="col">Aksi</th>

                </tr>
              </thead>

              <tbody>
                <?php
                        if( ! empty($DataKendaraan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                          $i = 1;
                          foreach($DataKendaraan as $data){

                            echo
                            '<tr>
                            <td>'.$i++.'</td>
                              <td>'.$data->id_kendaraan.'</td>
                             <td>'.$data->jenis.'</td>
                             <td>'.$data->nopol.'</td>
                              <td>'.$data->merk.'</td>
                              <td>'.$data->tahun.'</td>
                              <td>'.$data->ban.'</td>
                              <td>'.$data->oli.'</td>
                              <td>'.$data->air_radiator.'</td>
                              <td>'.$data->ac.'</td>
                              <td>
                                <a href="#" data-toggle="modal" data-target="#Edit'.$data->id_kendaraan.'"
                                class="btn btn-info">Edit
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  </a>

                                <a href="#" data-toggle="modal" data-target="#Delete'.$data->id_kendaraan.'" class="btn btn-danger">Delete </a>
                              </td>
                            </tr>';
                           include 'edit-kendaraan.php';
                          

                            $i++;
                          }
                        }else{ // Jika data  kosong
                          echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                        }
                        ?>
                      </tbody>
                    </table>

                  </div>
                </div>

                <script>
                    $(document).ready(function() {
                        $('#example').DataTable({
                            "scrollX": true
                        });
                    });
                </script>

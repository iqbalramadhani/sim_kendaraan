<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM Penggunaan Kendaraan</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/mdi/css/materialdesignicons.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.base.css')?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png')?>" />
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('css/jquery.dataTables.min.css'); ?>" />

  <script src="<?php echo base_url('assets/js/core/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/core/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js')?>"></script>


  <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
</head>
<body>
  <div class="container-scroller">
    <!-- navbar -->

    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="assets/images/logo.svg" alt="logo" /></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <div class="search-field d-none d-md-block">
          <form class="d-flex align-items-center h-100" action="#">
            <div class="input-group">
              <div class="input-group-prepend bg-transparent">
                <i class="input-group-text border-0 mdi mdi-magnify"></i>
              </div>
              <!-- <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects"> -->
            </div>
          </form>
        </div>
        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item nav-logout d-none d-lg-block">
            <a class="nav-link" href="<?php echo site_url('Login/logout'); ?>">
              <i class="mdi mdi-power"></i>
            </a>
          </li>

        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>

    <div class="container-fluid page-body-wrapper">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/Saw/')?>">
              <span class="menu-title">Beranda</span>
              <i class="mdi mdi-home menu-icon"></i>
            </a>
          </li>
        </ul>
      </nav>   
    <div class="main-panel">
      <div class="card-header">
        <h4>perhitungan Metode SAW</h4>
      </div>
      <div class="card-body">
        <div class="form-group">
<?php
      if ($hasil) {
      ?>

      <h1>Perhitungan Kriteria</h1>
      <table class="table" id="example">
        <thead>
          <tr>
            <b>
            <th rowspan="2">Alternatif</th>
            <th colspan="4">Kriteria</th>
          </b>
          </tr>
          <tr>
            <td scope="col">C1</td>
            <td scope="col">C2</td>
            <td scope="col">C3</td>
            <td scope="col">C4</td>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($kriteria as $q) {
          ?>
          <tr>
            <td scope="col"><?= $q->id_kendaraan; ?></td>
            <td scope="col"><?= $q->C1; ?></td>
            <td scope="col"><?= $q->C2; ?></td>
            <td scope="col"><?= $q->C3; ?></td>
            <td scope="col"><?= $q->C4; ?></td>
          </tr>
          <?php
        }
        ?>

      </tbody>
    </table>


    <h1>Matrix X</h1>
    <table class="table" border="2">
      <thead>
      <tr>
        <td rowspan="<?= count($x) + 1; ?>">X</td>
      </tr>
      <?php
      foreach ($x as $x1) {
      ?>
      <tr>
        <td scope="col"><?= $x1[0]; ?></td>
        <td scope="col"><?= $x1[1]; ?></td>
        <td scope="col"><?= $x1[2]; ?></td>
        <td scope="col"><?= $x1[3]; ?></td>
      </tr>
      <?php
    }
    ?>
  </thead>
  </table>

  <h1>Nilai r</h1>
  <table class="table" id="example">
    <tbody>
    <?php
    $no = 1;
    foreach ($r as $r1) {
    ?>
    <tr>
      <td><?= "r" . $no . "1 " . number_format($r1[0], 2, ',', ''); ?></td>
    </tr>
    <tr>
      <td><?= "r" . $no . "2 " . number_format($r1[1], 2, ',', ''); ?></td>
    </tr>
    <tr>
      <td><?= "r" . $no . "3 " . number_format($r1[2], 2, ',', ''); ?></td>
    </tr>
    <tr>
      <td><?= "r" . $no . "4 " . number_format($r1[3], 2, ',', ''); ?></td>
    </tr>
    <?php
    $no++;
  }
  ?>
</tbody>
</table>


<h1>Nilai V</h1>
<table class="table" border="1">
  <tbody>
  <?php
  $no = 1;
  foreach ($V as $v) {
  ?>
  <tr>
    <td><?= "V" . $no . " " . $v; ?></td>
  </tr>
  <?php
  $no++;
}
?>
</tbody>
</table>
<?php
}
?>

</div>
</div>  
<div class="content-wrapper">

</div>    
<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © Ilham Alhayyu 2019 <a href="https://www.bootstrapdash.com/" target="_blank">PT TIBAN INTEN</a>. All rights reserved.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted made with <i class="mdi mdi-heart text-danger"></i></span>
  </div>
</footer>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/js/vendor.bundle.base.js')?>"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="<?php echo base_url('assets/js/off-canvas.js')?>"></script>
<script src="<?php echo base_url('assets/js/hoverable-collapse.js')?>"></script>
<script src="<?php echo base_url('assets/js/misc.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/perfect-scrollbar.jquery.min.js')?>"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"" type="text/javascript"></script>
<script>
  $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartsPages();
    });
  </script>
  <!-- endinject -->
</body>
</html>


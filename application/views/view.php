<?php
function drawChart_open_all(status) {

    var PieChartData = $.ajax({
    type: 'POST',
    url: "<?php echo base_url(); ?>" + "dashboard/chart/mychart",
    data: { 'option':id_periode },  // <-- kept as option instead of val
    dataType:"json",
    global: false,              // <-- Added
    async:false,                // <-- Added
    success: function(data){    // <-- Added
    return data;                // <-- Added
    },
    error: function(xhr, textStatus, error){
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
    }
    }).responseText;

    // Create the data table.
    var data = new google.visualization.DataTable(PieChartData);
    var options = {  pieSliceText: 'value-and-percentage', };

    var chart = new google.visualization.PieChart(document.getElementById('open_new'));
    chart.draw(data, options);
  }
?>
<div><span> <b>Pie Chart<br /><br /></span></div>
<form>
<select name="id_periode" onchange="drawChart_open_all(this.value)">
<option value="WIP">WIP</option>
<option value="Close">Close</option>
</select>
</form>
<div id="open_new" class="chart"></div>

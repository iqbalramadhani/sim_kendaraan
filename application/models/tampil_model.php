<?php

class Tampil_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
public function Jum_kendaraan_perbulan()
    {
        $this->db->group_by('id_kendaraan');
        $this->db->select('id_kendaraan');
        $this->db->select("count(*) as total");
        return $this->db->from('booking')
          ->get()
          ->result();
    }
		public function GetAllData($table)
		{return $this->db->get($table)->result();
		}
		public function get_commo()
		{
			$query = $this->db->get('periodee');
			return $query->result();
		}
		public function getRecord($prov,$commo)
		{
			$this->db->select('kendaraan.nopol , periodee.bulan , kode_booking , tanggal_booking');
			$this->db->from('booking');
			$this->db->join('kendaraan' , 'booking.id_kendaraan = kendaraan.id_kendaraan');
			$this->db->join('periodee' , 'booking.id_periode = periodee.id_periode');
			$this->db->where(['booking.id_kendaraan'=>$prov, 'booking.id_periode'=>$commo]);
			$query = $this->db->get();
			return $query->result();
		}

    public function getTransportByPeriode($periode_id)
    {
        $this->db->group_by('id_kendaraan');
        $this->db->select('id_kendaraan');
        $this->db->select("count(*) as total");
        $this->db->where('id_periode', $periode_id);
        return $this->db->from('booking')
          ->get()
          ->result();
    }


}

?>

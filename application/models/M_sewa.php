<?php

/**
 * 
 */
class M_sewa extends CI_Model
{
	function __construct()
	{
		# code...
	}

	public function GetAllData($table)
	{

		return $this->db->get($table)->result();
	}


	public function GetDataSolo($table, $where)
	{
		return $this->db->where($where)->get($table)->result();
	}

	public function GetMultiWhere($table, $data, $kriteria)
	{
		return $this->db->where_in($kriteria, $data)->get($table)->result();;
	}

	public function GetDataJoin($query)
	{
		return $this->db->query("query")->result();
	}

	public function InsertData($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function UpdateData($table, $data, $where)
	{
		return $this->db->where($where)->update($table, $data);
	}

	public function DeleteData($table, $where)
	{
		return $this->db->where($where)->delete($table);
	}
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('sparepart', $data);
	}

	public function AddColumn($table, $columnname, $datatype)
	{
		$query = "ALTER TABLE " . $table . " ADD " . $columnname . " " . $datatype . " NOT NULL";

		// $result = ExecuteQuery($query, "Column Added successfully");
		$result = $this->db->query($query);
		return $result;
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SparepartModel extends CI_Model {
	// public function __construct() 
 //     {
 //           parent::__construct(); 
 //           $this->load->database();
 //     }
	// var $db;
	// var $table = "booking";
	// public function view(){
	// 	return $this->db->get('booking')->result(); // Tampilkan semua data yang ada di tabel siswa
	//}
	
	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload
		
		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;
	
		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data){
		$this->db->insert_batch('sparepart', $data);
	}
	public function GetAllData($table){

		return $this->db->get($table)->result();
	}


	public function GetDataSolo($table,$where){
		return $this->db->where($where)->get($table)->result();
	}

	public function GetDataJoin($query){
		return $this->db->query("query")->result();
	}

	public function InsertData($table,$data){
		return $this->db->insert($table,$data);
	}

	public function UpdateData($table,$data,$where){
		return $this->db->where($where)->update($table,$data);
	}

	public function DeleteData($table,$where){
		return $this->db->where($where)->delete($table);
	}


// public function buat_kode()   {
// 		  $this->db->select('RIGHT(booking.kode_booking,4) as kode', FALSE);
// 		  $this->db->order_by('kode_booking','DESC');    
// 		  $this->db->limit(1);    
// 		  $query = $this->db->get('booking');      //cek dulu apakah ada sudah ada kode di tabel.    
// 		  if($query->num_rows() <> 0){      
// 		   //jika kode ternyata sudah ada.      
// 		   $data = $query->row();      
// 		   $kode = intval($data->kode) + 1;    
// 		  }
// 		  else {      
// 		   //jika kode belum ada      
// 		   $kode = 1;    
// 		  }
// 		  $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
// 		  $kodejadi = "BUSP-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
// 		  return $kodejadi;  
// 	}

}
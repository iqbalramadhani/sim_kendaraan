 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');


 class Simple_login {

     // SET SUPER GLOBAL
     var $CI = NULL;

     /**
      * Class constructor
      *
      * @return   void
      */
     public function __construct() {
         $this->CI =& get_instance();
//         $this->load->model('m_data');
     }

     /*
     * cek username dan password pada table users, jika ada set session berdasar data user dari
     * table users.
     * @param string username dari input form
     * @param string password dari input form
     */
     public function update($username) {

         //cek username dan password
             //ambil data user berdasar email
             $row  = $this->CI->db->query('SELECT id_user,level FROM user where email = "'.$email.'"');
             $admin     = $row->row();
             $id     = $admin->id_user;
             $level = $admin->level;

             //set session user
             $this->CI->session->set_userdata('email', $email);
             $this->CI->session->set_userdata('id', $id);
             $this->CI->session->set_userdata('level', $level);
      }

     public function login($username, $password) {

         //cek email dan password
         $query = $this->CI->db->get_where('user',array('username'=>$username,'password' => md5($password)));

         if($query->num_rows() == 1) {
             //ambil data user berdasar email
             $row  = $this->CI->db->query('SELECT username,role FROM user where username = "'.$username.'"');
             $user= $row->row();
             //ambil data pasien berdasarkan email
             $username= $user->username;
             $role= $user->role;


             //set session user
             $this->CI->session->set_userdata('username', $username);
             $this->CI->session->set_userdata('id_login', uniqid(rand()));
             $this->CI->session->set_userdata('role', $role);
             //redirect ke website sesuai role
             redirect(site_url($role));
         }else{
             //redirect ke halaman login
             redirect(site_url());
         }
        return false;
      }
     public function cek_login() {
         if ($this->CI->session->userdata('role')) {
            redirect(site_url($this->CI->session->userdata('role')));
         }
     }
    public function cek_staff_mekanik(){
        //sudah login tapi bukan siswa
        if ($this->CI->session->userdata('role') != 'staff_mekanik') {
            redirect(site_url($this->CI->session->userdata('role')));
        }
        //sudah login dan siswa
        else if ($this->CI->session->userdata('role') == 'staff_mekanik') {
            ;
        }
        else{
            redirect(site_url());
        }

    }
    public function cek_kaop_bus(){
        //sudah login tapi bukan guru
        if ($this->CI->session->userdata('role') != 'kaop_bus') {
            redirect(site_url($this->CI->session->userdata('role')));
        }
        //sudah login dan guru
        else if ($this->CI->session->userdata('role') == 'kaop_bus') {
            ;
        }
        else{
            redirect(site_url());
        }
    }
    public function cek_admin_logistik(){
        //sudah login tapi bukan admin
        if ($this->CI->session->userdata('role') != 'kaadm_logistik') {
            redirect(site_url($this->CI->session->userdata('role')));
        }
        //sudah login dan siswa
        else if ($this->CI->session->userdata('role') == 'kaadm_logistik') {
            ;
        }
        else{
            redirect(site_url());
        }
    }
    public function cek_admin(){
        //sudah login tapi bukan siswa
        if ($this->CI->session->userdata('role') != 'admin') {
            redirect(site_url($this->CI->session->userdata('role')));
        }
        //sudah login dan siswa
        else if ($this->CI->session->userdata('role') == 'admin') {
            ;
        }
        else{
            redirect(site_url());
        }
    }
 }

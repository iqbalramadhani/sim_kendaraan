<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saw extends CI_Controller
{

	private $hasil;

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_sewa');
	}

	public function index()
	{
		$this->data['sidebar'] = "config/sidebar-hitung.php";
		$this->data['MainView'] = "Kaadm/saw";
		
		$hasil =  $this->M_sewa->GetAllData("kriteria");
		// echo count($this->hasil["query"]);
		$x = [];
		// $kolom = [0, 0, 0, 0];
		$C1 = [];
		$C2 = [];
		$C3 = [];
		$C4 = [];
		foreach ($hasil as $h) {
			array_push($x, [$h->C1, $h->C2, $h->C3, $h->C4]);
			array_push($C1, $h->C1);
			array_push($C2, $h->C2);
			array_push($C3, $h->C3);
			array_push($C4, $h->C4);
		}
		$kolom = [$C1, $C2, $C3, $C4];
// print_r($x);
		$r = [];
		for ($i = 0; $i < count($x); $i++) {
			$r1 = [];
			for ($j = 0; $j < count($x[$i]); $j++) {
				array_push($r1, $x[$i][$j] / max($kolom[$j]));
			}
			array_push($r, $r1);
		}

		$W = [0.2, 0.4, 0.3, 0.1];
		$V = [];
		for ($l = 0; $l < count($x); $l++) {
			array_push($V, $W[0] * $x[$l][0] + $W[1] * $x[$l][1] + $W[2] * $x[$l][2] + $W[3] * $x[$l][3]);
		}

		$this->hasil['x'] = $x;
		$this->hasil['r'] = $r;
		$this->hasil['V'] = $V;

		$this->hasil["kriteria"] =  $hasil;
		$this->hasil["query"] =  $this->M_sewa->GetAllData("kriteria");
		$this->hasil["hasil"] = true;
		$this->load->view("Kaadm/saw", $this->hasil);
		// print_r($V);
		
	}
}

			
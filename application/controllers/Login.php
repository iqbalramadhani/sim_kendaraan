
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
         parent::__construct();
         $this->load->library('form_validation');
         // $this->load->model('M_proses');
     }
	//merupakan sebuah kelas yang berisi aksi dari form seperti login,logout
	public function index()
	{
        $this->session->sess_destroy();
        redirect(base_url('index.php'));
    }
	public function login_here()
	{
		$valid = $this->form_validation;
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $valid->set_rules('username','username','required');
        $valid->set_rules('password','Password','required');
				//set message form validation
				$this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
						<div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
        if($valid->run()) {
        	$this->simple_login->login($username,$password);
        }
				else{
        	$this->load->view('login');
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect();
    }
    public function ganti_password($token){
        $this->data['MainView'] = "reset_password";

        $this->load->view('template');
    if($this->input->post("password") == $this->input->post("passconf")){
    //ganti password
        $data = array('token'         => $token);
        $ambil_email = $this->M_proses->GetDataSolo("tokens",$data);
        $data = array('password'         => md5($this->input->post("password")));
        $where = array('email' => $ambil_email[0]->id_user);
        $this->M_proses->UpdateData("user",$data,$where);

        echo "<a href=\".site_url().\">";
        echo "Password anda berhasil diperbaharui";
        echo "</a>";
        }
    }

    // public function ganti_password($token){
    //     $this->data['MainView'] = "reset_password";

    //     $this->load->view('template');
    // if($this->input->post("password") == $this->input->post("passconf")){
    // //ganti password
    //     $data = array('token'         => $token);
    //     $ambil_email = $this->M_proses->GetDataSolo("tokens",$data);
    //     $data = array('password'         => md5($this->input->post("password")));
    //     $where = array('email' => $ambil_email[0]->id_user);
    //     $this->M_proses->UpdateData("user",$data,$where);

    //     echo "<a href=\".site_url().\">";
    //     echo "Password anda berhasil diperbaharui";
    //     echo "</a>";
    //     }
    // }


}

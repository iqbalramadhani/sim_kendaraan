
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
	function __construct(){
       parent::__construct();
       $this->load->model('M_sewa');
       $this->simple_login->cek_admin();
   }
	//merupakan sebuah kelas yang berisi aksi dari form seperti login,logout,edit data dsb
	public function index(){
	 $this->data['sidebar'] = "config/sidebar-kendaraan.php";
	 $this->data['MainView'] = "Admin/v_datakendaraan";
	 if(!$this->input->post()){
			 //jika tidak ada post
			 $this->data['DataKendaraan'] = $this->M_sewa->GetAllData('kendaraan');
			 $this->load->view('Admin/template', $this->data);
	 }else if($this->input->post('add')){
			 //tambah data
			 $id_kendaraan  = $this->input->post('id_kendaraan');
			 $jenis = $this->input->post('jenis');
			 $nopol       = $this->input->post('nopol');
			 $merk = $this->input->post('merk');
			 $tahun = $this->input->post('tahun');


			 $data = array('id_kendaraan'   => $id_kendaraan,
					 'jenis' => $jenis,
					 'nopol' => $nopol,
					 'merk'   => $merk,
					 'tahun'  => $tahun);
			 $sql = $this->M_sewa->InsertData('kendaraan',$data);
			 if($sql){
					 echo'<script>alert("Data Berhasil Ditambahkan")</script>';
					 redirect(base_url('index.php/Admin/'),refresh);
			 }else{
					 echo'<script>alert("Data Gagal Ditambahkan")</script>';
					 redirect(base_url('index.php/Admin/'),refresh);
			 }

	 }else if($this->input->post('edit')){
			 //edit data

			 $id_kendaraan  = $this->input->post('id_kendaraan');
			 $jenis = $this->input->post('jenis');
			 $nopol       = $this->input->post('nopol');
			 $merk = $this->input->post('merk');
			 $tahun = $this->input->post('tahun');


			 $data = array('id_kendaraan'   => $id_kendaraan,
					 'jenis' => $jenis,
					 'nopol' => $nopol,
					 'merk'   => $merk,
					 'tahun'  => $tahun);

			 $where = array('id_kendaraan' => $id_kendaraan);
			 $sql = $this->M_sewa->UpdateData('kendaraan',$data, $where);
			 if($sql){
					 echo '<script>alert("Data berhasil diubah")</script>';
					 redirect(base_url('index.php/Admin/'),refresh);

			 }else{
					 echo '<script>alert("Data gagal diubah")</script>';
					 redirect(base_url('index.php/Admin/'),refresh);
			 }
	 }else if($this->input->post('delete')){
			 $id_kendaraan    = $this->input->post('id_kendaraan');
			 $where = array('id_kendaraan' => $id_kendaraan);
			 $sql = $this->M_sewa->DeleteData('kendaraan',$where);
			 if($sql){
					 echo '<script>alert("Data berhasil dihapus")</script>';
					 redirect(base_url('index.php/Admin/'),refresh);
			 }else{
					 echo '<script>alert("gagal updated")</script>';
					 redirect(base_url('index.php/Admin/'),refresh);
			 }
	 }
	}

	public function pegawai(){
	        $this->data['sidebar'] = "config/sidebar-pegawai.php";
	        $this->data['MainView'] = "Admin/v_pegawai";
	        if(!$this->input->post()){
	        //jika tidak ada post
	            $this->data['DataPegawai'] = $this->M_sewa->GetAllData('pegawai');
	            $this->load->view('Admin/template', $this->data);
	        }elseif ($this->input->post('add')) {
	            $id_pegawai = $this->input->post('id_pegawai');
	            $nama = $this->input->post('nama');
	            $jabatan = $this->input->post('jabatan');
	            $data = array('id_pegawai' => $id_pegawai,
	                'nama'   => $nama,
	                'jabatan' => $jabatan);
	            $sql = $this->M_sewa->InsertData('pegawai',$data);
	            if($sql){
	                echo'<script>alert("Data Berhasil Ditambahkan")</script>';
	                redirect(base_url('index.php/Admin/pegawai'),refresh);
	            }else{
	                echo'<script>alert("Data Gagal Ditambahkan")</script>';
	                redirect(base_url('index.php/Admin/pegawai'),refresh);
	            }
	        }elseif ($this->input->post('edit')) {
	            $id_pegawai = $this->input->post('id_pegawai');
	            $nama = $this->input->post('nama');
	            $jabatan = $this->input->post('jabatan');
	            $data = array('id_pegawai' => $id_pegawai,
	                'nama'   => $nama,
	                'jabatan' => $jabatan);
	            $where = array('id_pegawai' => $id_pegawai);
	            $sql = $this->M_sewa->UpdateData('pegawai',$data, $where);
	            if($sql){
	                echo'<script>alert("Data Berhasil Diubahn")</script>';
	                redirect(base_url('index.php/Admin/pegawai'),refresh);
	            }else{
	                echo'<script>alert("Data Gagal Diubah")</script>';
	                redirect(base_url('index.php/Admin/pegawai'),refresh);
	            }
	        }else if($this->input->post('delete')){
	            $id_pegawai  = $this->input->post('id_pegawai');
	            $where = array('id_pegawai' => $id_pegawai);
	            $sql = $this->M_sewa->DeleteData('pegawai',$where);
	            if($sql){
	                echo '<script>alert("Data berhasil dihapus")</script>';
	                redirect(base_url('index.php/Admin/pegawai'),refresh);
	            }else{
	                echo '<script>alert("gagal updated")</script>';
	                redirect(base_url('index.php/Admin/pegawai'),refresh);
	            }
	        }

	    }
	    public function pengguna(){
	        $this->data['sidebar'] = "config/sidebar-pengguna.php";
	        $this->data['MainView'] = "Admin/v_pengguna";
	        if(!$this->input->post()){
	        //jika tidak ada post
	            $this->data['DataPengguna'] = $this->M_sewa->GetAllData('user');
							 $this->data['DataPegawai'] = $this->M_sewa->GetAllData('pegawai');
	            $this->load->view('Admin/template', $this->data);
	        }elseif ($this->input->post('add')) {
	            $id_user = $this->input->post('id_user');
	            $username = $this->input->post('username');
	            $role = $this->input->post('role');
	            $password = $this->input->post('password');
	            $email = $this->input->post('email');
							$id_pegawai = $this->input->post('id_pegawai');


	            $data = array('id_user' => $id_user,
	                'username'   => $username,
	                'role' => $role,
	                'password' => $password,
	                'email'=>$email,
								'id_pegawai' => $id_pegawai);

	            $sql = $this->M_sewa->InsertData('user',$data);
	            if($sql){
	                echo'<script>alert("Data Berhasil Ditambahkan")</script>';
	                redirect(base_url('index.php/Admin/pengguna'),refresh);
	            }else{
	                echo'<script>alert("Data Gagal Ditambahkan")</script>';
	                redirect(base_url('index.php/Admin/pengguna'),refresh);
	            }
	        }elseif ($this->input->post('edit')) {
	            $id_user = $this->input->post('id_user');
	            $username = $this->input->post('username');
	            $role = $this->input->post('role');
	            $password = $this->input->post('password');
	            $email = $this->input->post('email');
							$id_pegawai = $this->input->post('id_pegawai');
	            $data = array('id_user' => $id_user,
	                'username'   => $username,
	                'role' => $role,
	                'password' => $password,
	                'email'=>$email,
								'id_pegawai' => $id_pegawai);

	            $where = array('id_user' => $id_user);
	            $sql = $this->M_sewa->UpdateData('user',$data, $where);
	            if($sql){
	                echo'<script>alert("Data Berhasil Diubah")</script>';
	                redirect(base_url('index.php/Admin/pengguna'),refresh);
	            }else{
	                echo'<script>alert("Data Gagal Diubah")</script>';
	                redirect(base_url('index.php/Admin/pengguna'),refresh);
	            }
	        }else if($this->input->post('delete')){
	            $id_user  = $this->input->post('id_user');
	            $where = array('id_user' => $id_user);
	            $sql = $this->M_sewa->DeleteData('user',$where);
	            if($sql){
	                echo '<script>alert("Data berhasil dihapus")</script>';
	                redirect(base_url('index.php/Admin/pengguna'),refresh);
	            }else{
	                echo '<script>alert("gagal updated")</script>';
	                redirect(base_url('index.php/Admin/pengguna'),refresh);
	            }
	        }

	    }

}

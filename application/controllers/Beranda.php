<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('tampil_model');
	}

	function index()
	{
		$data['hasil'] = $this->tampil_model->Jum_kendaraan_perbulan();
		$data['DataPeriode'] = $this->tampil_model->GetAllData('periodee');
		// print_r($data['DataPeriode']);
		// die();
		$this->load->view('Admin/GetRecord', $data);
	}

	function getPeriode()
	{

		$this->data['DataPeriode'] = $this->tampil_model->GetAllData('periodee');
	}

    function updateDataByPeriode()
    {
        $data['result'] = $this->tampil_model->getTransportByPeriode($this->input->post('periode'));

       //add the header here
        header('Content-Type: application/json');
        echo json_encode( $data );
    }

}

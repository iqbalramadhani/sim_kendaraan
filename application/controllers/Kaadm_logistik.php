<?php
defined('BASEPATH') or exit('id_maintain direct script access allowed');

class Kaadm_logistik extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_sewa');
        $this->simple_login->cek_admin_logistik();
    }

    public function index()
    {
        $this->data['sidebar'] = "config/sidebar-kriteria.php";
        $this->data['MainView'] = "Kaadm/v_kriteria";
        if (!$this->input->post()) {
            //jika tidak ada post
            $this->data['DataKriteria'] = $this->M_sewa->GetAllData('kriteria');
            $this->data['DataKendaraan'] = $this->M_sewa->GetAllData('kendaraan');
            $this->load->view('Kaadm/template', $this->data);
        } elseif ($this->input->post('add')) {
            $id_kriteria = $this->input->post('id_kriteria');
            $nopol = $this->input->post('nopol');
            $C1 = $this->input->post('C1');
            $C2 = $this->input->post('C2');
            $C3 = $this->input->post('C3');
            $C4 = $this->input->post('C4');

            $data = array(
                'id_kriteria' => $id_kriteria,
                'nopol' => $nopol,
                'C1' => $C1,
                'C2' => $C2,
                'C3' => $C3,
                'C4' => $C4
            );
            $sql = $this->M_sewa->InsertData('kriteria', $data);
            if ($sql) {
                echo '<script>alert("Data Berhasil Ditambahkan")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            } else {
                echo '<script>alert("Data Gagal Ditambahkan")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            }
        } elseif ($this->input->post('edit')) {
            $id_kriteria = $this->input->post('id_kriteria');
            $nopol = $this->input->post('nopol');
            $C1 = $this->input->post('C1');
            $C2 = $this->input->post('C2');
            $C3 = $this->input->post('C3');
            $C4 = $this->input->post('C4');

            $data = array(
                'id_kriteria' => $id_kriteria,
                'nopol' => $nopol,
                'C1' => $C1,
                'C2' => $C2,
                'C3' => $C3,
                'C4' => $C4
            );
            $where = array('id_kriteria' => $id_kriteria);

            $sql = $this->M_sewa->UpdateData('kriteria', $data, $where);
            if ($sql) {
                echo '<script>alert("Data Berhasil Diubahn")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            } else {
                echo '<script>alert("Data Gagal Diubah")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            }
        } else if ($this->input->post('delete')) {
            $id_kriteria  = $this->input->post('id_kriteria');
            $where = array('id_kriteria' => $id_kriteria);
            $sql = $this->M_sewa->DeleteData('kriteria', $where);
            if ($sql) {
                echo '<script>alert("Data berhasil dihapus")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            } else {
                echo '<script>alert("gagal updated")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            }
        } else if ($this->input->post('Addnew')) {

            // echo "bisa";
            $sql = $this->M_sewa->AddColumn('kriteria', $this->input->post('kolom'), 'INT');
            if ($sql) {
                echo '<script>alert("Field berhasil ditambahkan")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            } else {
                echo '<script>alert("gagal updated")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            }
        } else if ($this->input->post('delete')) {
            $id_kriteria  = $this->input->post('id_kriteria');
            $where = array('id_kriteria' => $id_kriteria);
            $sql = $this->M_sewa->DeleteData('kriteria', $where);
            if ($sql) {
                echo '<script>alert("Data berhasil dihapus")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            } else {
                echo '<script>alert("gagal updated")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            }
        } else if ($this->input->post('Addnew')) {

            $sql = $this->M_sewa->AddColumn('kriteria', 'C5', 'integer');
            if ($sql) {
                echo '<script>alert("Field berhasil ditambahkan")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            } else {
                echo '<script>alert("gagal updated")</script>';
                redirect(base_url('index.php/Kaadm_logistik/'), refresh);
            }
        }
    }
}

<?php
defined('BASEPATH') OR exit('id_maintain direct script access allowed');

class Staff_Mekanik extends MY_Controller {
 private $filename = "Sparepart";
    // private $filename = "MONITORING";
 function __construct(){
   parent::__construct();
   $this->load->model('M_sewa');
   $this->simple_login->cek_staff_mekanik();
}

public function index(){
    // $data['kodeunik'] = $this->BookingModel->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong
    $this->data['sidebar'] = "config/sidebar-maintain.php";
    $this->data['MainView'] = "Staff/v_sparepart";
    if(!$this->input->post()){
        //jika tidak ada post
        $this->data['DataSparepart'] = $this->M_sewa->GetAllData('sparepart');
        $this->data['DataKendaraan'] = $this->M_sewa->GetAllData('kendaraan');
        $this->load->view('Staff/template', $this->data);
    }elseif ($this->input->post('add')) {
        $id_maintain = $this->input->post('id_maintain');
        $nopol  = $this->input->post('nopol');
        $id_periode = $this->input->post('id_periode');
        $tanggal = $this->input->post('tanggal');
        $nomor = $this->input->post('nomor');
        $nama_barang = $this->input->post('nama_barang');
        $deskripsi = $this->input->post('deskripsi');
        $quantity = $this->input->post('quantity');
        $unit = $this->input->post('unit');
        $total = $this->input->post('total');
        $data = array('id_maintain' => $id_maintain,
                    'nopol'   => $nopol,
                    'id_periode' => $id_periode,
                    'tanggal' => $tanggal,
                    'nomor' => $nomor,
                    'nama_barang' => $nama_barang,
                    'deskripsi' => $deskripsi,
                    'quantity' => $quantity,
                    'unit' => $unit,
                    'total' => $total);
        $sql = $this->M_sewa->InsertData('sparepart',$data);
        if($sql){
            echo'<script>alert("Data Berhasil Ditambahkan")</script>';
            redirect(base_url('index.php/Staff_Mekanik/'),refresh);
        }else{
            echo'<script>alert("Data Gagal Ditambahkan")</script>';
            redirect(base_url('index.php/Staff_Mekanik/'),refresh);
        }
    }
}

public function monitor_ban(){
    // $data['kodeunik'] = $this->BookingModel->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong
    $this->data['sidebar'] = "config/sidebar-monitoring.php";
    $this->data['MainView'] = "Staff/v_monitor_ban";
    if(!$this->input->post()){
        //jika tidak ada post
        $this->data['DataMonitorBan'] = $this->M_sewa->GetAllData('penggunaan_ban');
        $this->data['DataKendaraan'] = $this->M_sewa->GetAllData('kendaraan');
        $this->data['DataSopir'] = $this->M_sewa->GetAllData('sopir');
        $this->load->view('Staff/template', $this->data);
    }elseif ($this->input->post('add')) {
        $id_penggunaan_ban = $this->input->post('id_penggunaan_ban');
        $id_kendaraan = $this->input->post('id_kendaraan');
        $id_periode = $this->input->post('id_periode');
        $id_sopir = $this->input->post('id_sopir');
        $tgl_pakai = $this->input->post('tgl_pakai');
        $id_ban = $this->input->post('id_ban');
        $km_pakai = $this->input->post('km_pakai');
        $tgl_lepas = $this->input->post('tgl_lepas');
        $km_lepas = $this->input->post('km_lepas');
        $balance = $this->input->post('balance');
        $shor_over= $this->input->post('shor_over');
        $km= $this->input->post('km');
        $shor_over2= $this->input->post('shor_over2');
        $ban_bekas= $this->input->post('ban_bekas');

        $data = array('id_penggunaan_ban' => $id_penggunaan_ban,
            'id_kendaraan'   => $id_kendaraan,
            'id_periode' => $id_periode,
            'id_sopir' => $id_sopir,
            'tgl_pakai' => $tgl_pakai,
            'id_ban'=>$id_ban,
            'km_pakai' => $km_pakai,
            'tgl_lepas' => $tgl_lepas,
            'km_lepas' => $km_lepas,
            'balance' => $balance,
            'shor_over' => $shor_over,
            'km' => $km,
            'shor_over2' =>$shor_over2 ,
            'ban_bekas' => $ban_bekas);
        $sql = $this->M_sewa->InsertData('penggunaan_ban',$data);
        if($sql){
            echo'<script>alert("Data Berhasil Ditambahkan")</script>';
            redirect(base_url('index.php/Staff_Mekanik/monitor_ban'),refresh);
        }else{
            echo'<script>alert("Data Gagal Ditambahkan")</script>';
            redirect(base_url('index.php/Staff_Mekanik/monitor_ban'),refresh);
        }
    }elseif ($this->input->post('edit')) {
        $id_penggunaan_ban = $this->input->post('id_penggunaan_ban');
        $id_kendaraan  = $this->input->post('id_kendaraan');
        $id_periode = $this->input->post('id_periode');
        $id_sopir = $this->input->post('id_sopir');
        $tgl_pakai = $this->input->post('tgl_pakai');
        $id_ban = $this->input->post('id_ban');
        $km_pakai = $this->input->post('km_pakai');
        $tgl_lepas = $this->input->post('tgl_lepas');
        $km_lepas = $this->input->post('km_lepas');
        $balance = $this->input->post('balance');
        $shor_over= $this->input->post('shor_over');
        $km= $this->input->post('km');
        $shor_over2= $this->input->post('shor_over2');
        $ban_bekas= $this->input->post('ban_bekas');

        $data = array('id_penggunaan_ban' => $id_penggunaan_ban,
            'id_kendaraan'   => $id_kendaraan,
            'id_periode' => $id_periode,
            'id_sopir' => $id_sopir,
            'tgl_pakai' => $tgl_pakai,
            'id_ban'=>$id_ban,
            'km_pakai' => $km_pakai,
            'tgl_lepas' => $tgl_lepas,
            'km_lepas' => $km_lepas,
            'balance' => $balance,
            'shor_over' => $shor_over,
            'km' => $km,
            'shor_over2' =>$shor_over2 ,
            'ban_bekas' => $ban_bekas);
        $where = array('id_penggunaan_ban' => $id_penggunaan_ban);
        $sql = $this->M_sewa->UpdateData('penggunaan_ban',$data, $where);
        if($sql){
            echo'<script>alert("Data Berhasil Diubah")</script>';
            redirect(base_url('index.php/Staff_Mekanik/monitor_ban'),refresh);
        }else{
            echo'<script>alert("Data Gagal Diubah")</script>';
            redirect(base_url('index.php/Staff_Mekanik/monitor_ban'),refresh);
        }
    }else if($this->input->post('delete')){
        $id_penggunaan_ban   = $this->input->post('id_penggunaan_ban');
        $where = array('id_penggunaan_ban' => $id_penggunaan_ban);
        $sql = $this->M_sewa->DeleteData('penggunaan_ban',$where);
        if($sql){
            echo '<script>alert("Data berhasil dihapus")</script>';
            redirect(base_url('index.php/Staff_Mekanik/monitor_ban'),refresh);
        }else{
            echo '<script>alert("gagal updated")</script>';
            redirect(base_url('index.php/Staff_Mekanik/monitor_ban'),refresh);
        }
    }

}
// //ini fungsi import
// public function form(){
//     $this->data['sidebar'] = "config/sidebar.php";
//     $this->data['MainView'] = "Staff/form";
//
//         $data = array(); // Buat variabel $data sebagai array
//
//         if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
//             // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
//             $upload = $this->M_sewa->upload_file($this->filename);
//
//             if($upload['result'] == "success"){ // Jika proses upload sukses
//                 // Load plugin PHPExcel nya
//                 include APPPATH.'third_party/PHPExcel/PHPExcel.php';
//
//                 $excelreader = new PHPExcel_Reader_Excel2007();
//                 $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
//                 $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
//
//                 // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
//                 // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
//                 $data['sheet'] = $sheet;
//             }else{ // Jika proses upload gagal
//                 $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
//             }
//         }
//
//         $this->load->view('Staff/template', ['data' => $data]);
//     }
//
//     public function import(){
//         set_time_limit(3600);
//         // Load plugin PHPExcel nya
//         include APPPATH.'third_party/PHPExcel/PHPExcel.php';
//
//         $excelreader = new PHPExcel_Reader_Excel2007();
//         $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
//         $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
//
//         // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
//         $data = array();
//
//         $numrow = 1;
//         foreach($sheet as $row){
//             // Cek $numrow apakah lebih dari 1
//             // Artinya karena baris pertama adalah nama-nama kolom
//             // Jadi dilewat saja, tidak usah diimport
//             if($numrow > 1){
//                 // Kita push (add) array data ke variabel data
//                 array_push($data, array(
//                     'id_maintain'=>$row['A'], // Insert data id_maintain dari kolom A di excel
//                     'id_kendaraan'=>$row['B'], // Insert data nama dari kolom B di excel
//                     'tanggal'=>$row['C'], //Insert data tanggal booking dari kolom B di excel
//                     'nomor'=>$row['D'], // Insert data tujuan dari kolom C di excel
//                     'nama_barang'=>$row['E'], // Insert data jemput dari kolom C di excel
//                     'deskripsi'=>$row['F'], // Insert data jam dari kolom C di excel
//                     'quantity'=>$row['G'], // Insert data tgl pergi dari kolom C di excel
//                     'unit'=>$row['H'], // Insert data tgl pulang dari kolom C di excel
//                     'total'=>$row['I'], // Insert data bus dari kolom C di excel
//                 ));
//             }
//
//             $numrow++; // Tambah 1 setiap kali looping
//         }
//
//
//         $this->M_sewa->insert_multiple($data);
//
//         redirect("Staff_Mekanik"); // Redirect ke halaman awal (ke controller siswa fungsi index)
//     }
//
//     public function form_ban(){
//         $this->data['sidebar'] = "config/sidebar.php";
//         $this->data['MainView'] = "Staff/form_ban";
//
//         $data = array(); // Buat variabel $data sebagai array
//
//         if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
//             // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
//             $upload = $this->M_sewa->upload_file($this->filename);
//
//             if($upload['result'] == "success"){ // Jika proses upload sukses
//                 // Load plugin PHPExcel nya
//                 include APPPATH.'third_party/PHPExcel/PHPExcel.php';
//
//                 $excelreader = new PHPExcel_Reader_Excel2007();
//                 $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
//                 $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
//
//                 // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
//                 // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
//                 $data['sheet'] = $sheet;
//             }else{ // Jika proses upload gagal
//                 $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
//             }
//         }
//
//         $this->load->view('Staff/template', ['data' => $data]);
//     }
//
//     public function import_ban(){
//         set_time_limit(3600);
//         // Load plugin PHPExcel nya
//         include APPPATH.'third_party/PHPExcel/PHPExcel.php';
//
//         $excelreader = new PHPExcel_Reader_Excel2007();
//         $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
//         $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
//
//         // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
//         $data = array();
//
//         $numrow = 1;
//         foreach($sheet as $row){
//             // Cek $numrow apakah lebih dari 1
//             // Artinya karena baris pertama adalah nama-nama kolom
//             // Jadi dilewat saja, tidak usah diimport
//             if($numrow > 1){
//                 // Kita push (add) array data ke variabel data
//                 array_push($data, array(
//                     'id_penggunaan_ban'=>$row['A'], // Insert data id_maintain dari kolom A di excel
//                     'id_kendaraan'=>$row['B'], // Insert data nama dari kolom B di excel
//                     'id_sopir'=>$row['C'], //Insert data tanggal booking dari kolom B di excel
//                     'tgl_pakai'=>$row['D'], // Insert data tujuan dari kolom C di excel
//                     'id_ban'=>$row['E'], // Insert data jemput dari kolom C di excel
//                     'merk'=>$row['F'], // Insert data jam dari kolom C di excel
//                     'km_pakai'=>$row['G'], // Insert data tgl pergi dari kolom C di excel
//                     'harga'=>$row['H'], // Insert data tgl pulang dari kolom C di excel
//                     'tgl_lepas'=>$row['I'], // Insert data bus dari kolom C di excel
//                     'km_lepas'=>$row['J'],
//                     'balance'=>$row['K'],
//                     'harga_klaim'=>$row['L'],
//                     'shor_over'=>$row['M'],
//                     'km'=>$row['N'],
//                     'shor_over2'=>$row['O'],
//                     'ban_bekas'=>$row['P'],
//                 ));
//             }
//
//             $numrow++; // Tambah 1 setiap kali looping
//         }
//
//
//         $this->M_sewa->insert_multiple($data);
//
//         redirect("Staff_Mekanik/monitor_ban"); // Redirect ke halaman awal (ke controller siswa fungsi index)
//     }
public function Kendaraan(){
 $this->data['sidebar'] = "config/sidebar-kendaraan.php";
 $this->data['MainView'] = "Staff/v_datakendaraan";
 if(!$this->input->post()){
     //jika tidak ada post
     $this->data['DataKendaraan'] = $this->M_sewa->GetAllData('kendaraan');
     $this->load->view('Staff/template', $this->data);
 }else if($this->input->post('add')){
     //tambah data
     $id_kendaraan  = $this->input->post('id_kendaraan');
     $jenis = $this->input->post('jenis');
     $nopol       = $this->input->post('nopol');
     $merk = $this->input->post('merk');
     $tahun = $this->input->post('tahun');
     $oli = $this->input->post('oli');
     $ban      = $this->input->post('ban');
     $air_radiator = $this->input->post('air_radiator');
     $ac = $this->input->post('ac');

     $data = array('id_kendaraan'   => $id_kendaraan,
         'jenis' => $jenis,
         'nopol' => $nopol,
         'merk'   => $merk,
         'tahun'  => $tahun,
         'oli' => $oli,
         'ban' => $ban,
         'air_radiator'   => $air_radiator,
         'ac'  => $ac);
     $sql = $this->M_sewa->InsertData('kendaraan',$data);
     if($sql){
         echo'<script>alert("Data Berhasil Ditambahkan")</script>';
         redirect(base_url('index.php/Staff_Mekanik/kendaraan'),refresh);
     }else{
         echo'<script>alert("Data Gagal Ditambahkan")</script>';
         redirect(base_url('index.php/Staff_Mekanik/kendaraan'),refresh);
     }

 }else if($this->input->post('edit')){
     //edit data

     $id_kendaraan  = $this->input->post('id_kendaraan');
     $jenis = $this->input->post('jenis');
     $nopol       = $this->input->post('nopol');
     $merk = $this->input->post('merk');
     $tahun = $this->input->post('tahun');
     $oli = $this->input->post('oli');
     $ban      = $this->input->post('ban');
     $air_radiator = $this->input->post('air_radiator');
     $ac = $this->input->post('ac');

     $data = array('id_kendaraan'   => $id_kendaraan,
         'jenis' => $jenis,
         'nopol' => $nopol,
         'merk'   => $merk,
         'tahun'  => $tahun,
         'oli' => $oli,
         'ban' => $ban,
         'air_radiator'   => $air_radiator,
         'ac'  => $ac);

     $where = array('id_kendaraan' => $id_kendaraan);
     $sql = $this->M_sewa->UpdateData('kendaraan',$data, $where);
     if($sql){
         echo '<script>alert("Data berhasil diubah")</script>';
         redirect(base_url('index.php/Staff_Mekanik/kendaraan'),refresh);

     }else{
         echo '<script>alert("Data gagal diubah")</script>';
         redirect(base_url('index.php/Staff_Mekanik/kendaraan'),refresh);
     }
 }else if($this->input->post('delete')){
     $id_kendaraan    = $this->input->post('id_kendaraan');
     $where = array('id_kendaraan' => $id_kendaraan);
     $sql = $this->M_sewa->DeleteData('kendaraan',$where);
     if($sql){
         echo '<script>alert("Data berhasil dihapus")</script>';
         redirect(base_url('index.php/Staff_Mekanik/kendaraan'),refresh);
     }else{
         echo '<script>alert("gagal updated")</script>';
         redirect(base_url('index.php/Staff_Mekanik/kendaraan'),refresh);
     }
 }
}


}

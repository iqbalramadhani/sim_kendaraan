<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();

		// $this->load->model('M_Account');
		// $this->load->model('M_proses');
		// $this->load->helper('string');
		//$this->simple_login->cek_login();
	}
	public function index()
	{
		if ($this->session->userdata('role') == 'kaop_dump') {
			redirect(site_url('Dumptruck'));
		} else if ($this->session->userdata('role') == 'kaop_bus') {
			redirect(site_url('Bus'));
		} else if ($this->session->userdata('role') == 'kaop_tongkang') {
			redirect(site_url('Tongkang'));
		} elseif ($this->session->userdata('role') == 'kaadm_logistik') {
			redirect(site_url('Admin_logistik'));
		} else {
			$this->load->view('login');
		}
	}

	public function reset_password()
	{
		$this->load->view('reset_password');
	}
	public function reset_password_token($token)
	{
		$this->load->view('admin/template', $this->data);

		if ($this->M_proses->get_token($token) == true) {

			//jika token ada
			$this->data['token'] = $token;
			$this->data['MainView'] = "lupa_password";
			// $this->load->view('reset_password',$this->data);     
		} else {
			//jika token ujian tidak ada
			echo "verifikasi email tidak ada";
		}
	}

	public function reset_password_validation()
	{
		$this->data['MainView'] = "lupa_password";
		$this->load->view('template');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
		if ($this->form_validation->run()) {

			$email = $this->input->post('email');
			$reset_key = random_string('alnum', 50);

			$data = array(
				'token'         => $reset_key,
				'id_user'      => $email
			);
			$sql = $this->M_proses->InsertData('tokens', $data);

			if ($this->M_Account->update_reset_key($email, $reset_key)) {

				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol'] = "smtp";
				$config['mailtype'] = "html";
				$config['smtp_host'] = "ssl://smtp.gmail.com"; //pengaturan smtp
				$config['smtp_port'] = "465";
				$config['smtp_timeout'] = "5";
				$config['smtp_user'] = "aisahicha75@gmail.com"; // isi dengan email kamu
				$config['smtp_pass'] = ""; // isi dengan password kamu
				$config['crlf'] = "\r\n";
				$config['newline'] = "\r\n";
				$config['wordwrap'] = TRUE;
				//memanggil library email dan set konfigurasi untuk pengiriman email

				$this->email->initialize($config);
				//konfigurasi pengiriman
				$this->email->from($config['smtp_user']);
				$this->email->to($this->input->post('email'));
				$this->email->subject("Reset your password");

				$message = "<p>Anda melakukan permintaan reset password</p>";
				$message .= "<a href='" . site_url('welcome/reset_password_token/' . $reset_key) . "'>klik reset password</a>";
				$this->email->message($message);

				if ($this->email->send()) {
					echo "silahkan cek email <b>" . $this->input->post('email') . '</b> untuk melakukan reset password';
				} else {
					echo "Berhasil melakukan registrasi, gagal mengirim verifikasi email";
				}

				echo "<br><br><a href='" . site_url("member-login") . "'>Kembali ke Menu Login</a>";
			} else {
				die("Email yang anda masukan belum terdaftar");
			}
		} else {
			$this->load->view('reset_password');
		}
	}

	public function test()
	{
		$data['haha'] = $this->AdminModel->yolo()->result();

		$this->load->view('admin/kuy', $data);
	}
}

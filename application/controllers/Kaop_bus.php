<?php
defined('BASEPATH') OR exit('kode_booking direct script access allowed');

class Kaop_Bus extends MY_Controller {
    private $filename = "Book";
    function __construct(){
       parent::__construct();
       $this->load->model('BookingModel');
       $this->simple_login->cek_kaop_bus();
   }
   public function index(){
    // $data['kodeunik'] = $this->BookingModel->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong
    $this->data['sidebar'] = "config/sidebar-booking.php";
    $this->data['MainView'] = "OpBus/v_databooking";
    if(!$this->input->post()){
        //jika tidak ada post
        $this->data['DataBookingBus'] = $this->BookingModel->GetAllData('booking');
        $where =array('jenis' =>  'bus' , 'status' => 'avaible');
        $this->data['DataBus'] = $this->BookingModel->GetDataSolo('kendaraan', $where);
        $this->load->view('OpBus/template', $this->data);
    }else if($this->input->post('add')){
        //tambah data
        $kode_booking = $this->input->post('kode_booking');
        $id_periode = $this->input->post('id_periode');
        $nama  = $this->input->post('nama');
        $tgl_booking = $this->input->post('tgl_booking');
        $tujuan = $this->input->post('tujuan');
        $jemput = $this->input->post('jemput');
        $jam = $this->input->post('jam');
        $tgl_berangkat = $this->input->post('tgl_berangkat');
        $tgl_pulang = $this->input->post('tgl_pulang');
        $id_kendaraan = $this->input->post('id_kendaraan');

        $data = array('kode_booking' => $kode_booking,
        'id_periode' => $id_periode,
            'nama'   => $nama,

            'tgl_booking'   => $tgl_booking,
            'tujuan'  => $tujuan,
            'jemput' => $jemput,
            'jam' => $jam,
            'tgl_berangkat' => $tgl_berangkat,
            'tgl_pulang' => $tgl_pulang,
            'id_kendaraan' => $id_kendaraan);
        $sql = $this->BookingModel->InsertData('booking',$data);
        if($sql){
            echo'<script>alert("Data Berhasil Ditambahkan")</script>';
            redirect(base_url('index.php/kaop_bus'),refresh);
        }else{
            echo'<script>alert("Data Gagal Ditambahkan")</script>';
            redirect(base_url('index.php/kaop_bus'),refresh);
        }
    }else if($this->input->post('edit')){
        //edit data
        $kode_booking = $this->input->post('kode_booking');
        $id_periode = $this->input->post('id_periode');
        $nama  = $this->input->post('nama');
        $tgl_booking = $this->input->post('tgl_booking');

        $tujuan = $this->input->post('tujuan');
        $jemput = $this->input->post('jemput');
        $jam = $this->input->post('jam');
        $tgl_berangkat = $this->input->post('tgl_berangkat');
        $tgl_pulang = $this->input->post('tgl_pulang');
        $id_kendaraan = $this->input->post('id_kendaraan');

        $data = array('kode_booking' => $kode_booking,
            'id_periode' => $id_periode,
            'nama'   => $nama,
            'tgl_booking'   => $tgl_booking,
            'id_periode' => $id_periode,
            'tujuan'  => $tujuan,
            'jemput' => $jemput,
            'jam' => $jam,
            'tgl_berangkat' => $tgl_berangkat,
            'tgl_pulang' => $tgl_pulang,
            'id_kendaraan' => $id_kendaraan);

        $where = array('kode_booking' => $kode_booking);
        $sql = $this->BookingModel->UpdateData('booking',$data, $where);
        if($sql){
            echo '<script>alert("Data berhasil diubah")</script>';
            redirect(base_url('index.php/kaop_bus'),refresh);

        }else{
            echo '<script>alert("Data gagal diubah")</script>';
            redirect(base_url('index.php/kaop_bus'),refresh);
        }
    }else if($this->input->post('delete')){
        $kode_booking    = $this->input->post('kode_booking');
        $where = array('kode_booking' => $kode_booking);
        $sql = $this->BookingModel->DeleteData('booking',$where);
        if($sql){
            echo '<script>alert("Data berhasil dihapus")</script>';
            redirect(base_url('index.php/Kaop_bus'),refresh);
        }else{
            echo '<script>alert("gagal updated")</script>';
            redirect(base_url('index.php/Kaop_bus'),refresh);
        }
    }
}

//ini fungsi import
public function form(){
    $this->data['sidebar'] = "config/sidebar.php";
    $this->data['MainView'] = "OpBus/form";

        $data = array(); // Buat variabel $data sebagai array

        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            $upload = $this->BookingModel->upload_file($this->filename);

            if($upload['result'] == "success"){ // Jika proses upload sukses
                // Load plugin PHPExcel nya
                include APPPATH.'third_party/PHPExcel/PHPExcel.php';

                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet;
            }else{ // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }

        $this->load->view('Booking/template', ['data' => $data]);
    }

    public function import(){
        set_time_limit(3600);
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();

        $numrow = 1;
        foreach($sheet as $row){
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
                // Kita push (add) array data ke variabel data
                array_push($data, array(
                    'kode_booking'=>$row['A'], // Insert data kode_booking dari kolom A di excel
                    'id_periode'=>$row['B'],
                    'nama'=>$row['C'], // Insert data nama dari kolom B di excel
                    'tgl_booking'=>$row['D'], //Insert data tanggal booking dari kolom B di excel
                    'tujuan'=>$row['E'], // Insert data tujuan dari kolom C di excel
                    'jemput'=>$row['F'], // Insert data jemput dari kolom C di excel
                    'jam'=>$row['G'], // Insert data jam dari kolom C di excel
                    'tgl_berangkat'=>$row['H'], // Insert data tgl pergi dari kolom C di excel
                    'tgl_pulang'=>$row['I'], // Insert data tgl pulang dari kolom C di excel
                    'id_kendaraan'=>$row['J'], // Insert data bus dari kolom C di excel
                ));
            }

            $numrow++; // Tambah 1 setiap kali looping
        }


        $this->BookingModel->insert_multiple($data);

        redirect("kaop_bus"); // Redirect ke haid_perioden awal (ke controller siswa fungsi index)
    }

    public function kendaraan(){
    $this->data['sidebar'] = "config/sidebar-kendaraan.php";
    $this->data['MainView'] = "OpBus/v_datakendaraan";
    if(!$this->input->post()){
        //jika tidak ada post

        $where =array('jenis' =>  'bus');
        $this->data['DataKendaraanBus'] = $this->BookingModel->GetDataSolo('kendaraan', $where);
        $this->load->view('OpBus/template', $this->data);
    }else if($this->input->post('add')){
        //tambah data
        $id_kendaraan  = $this->input->post('id_kendaraan');
        $jenis = $this->input->post('jenis');
        $nopol       = $this->input->post('nopol');
        $merk = $this->input->post('merk');
        $tahun = $this->input->post('tahun');
        // $status = $this->input->post('status');

        $data = array('id_kendaraan'   => $id_kendaraan,
            'jenis' => $jenis,
            'nopol' => $nopol,
            'merk'   => $merk,
            'tahun'  => $tahun);
            // 'status' => $status);
        $sql = $this->BookingModel->InsertData('kendaraan',$data);
        if($sql){
            echo'<script>alert("Data Berhasil Ditambahkan")</script>';
            redirect(base_url('index.php/Kaop_bus/kendaraan'),refresh);
        }else{
            echo'<script>alert("Data Gagal Ditambahkan")</script>';
            redirect(base_url('index.php/Kaop_bus/kendaraan'),refresh);
        }

    }else if($this->input->post('edit')){
        //edit data

        $id_kendaraan  = $this->input->post('id_kendaraan');
        $jenis = $this->input->post('jenis');
        $nopol       = $this->input->post('nopol');
        $merk = $this->input->post('merk');
        $tahun = $this->input->post('tahun');
        $status = $this->input->post('status');

        $data = array('id_kendaraan'   => $id_kendaraan,
            'jenis' => $jenis,
            'nopol' => $nopol,
          'merk'   => $merk,
          'tahun'  => $tahun,
          'status' => $status);

        $where = array('id_kendaraan' => $id_kendaraan);
        $sql = $this->BookingModel->UpdateData('kendaraan',$data, $where);
        if($sql){
            echo '<script>alert("Data berhasil diubah")</script>';
            redirect(base_url('index.php/Kaop_bus/kendaraan'),refresh);

        }else{
            echo '<script>alert("Data gagal diubah")</script>';
            redirect(base_url('index.php/Kaop_bus/kendaraan'),refresh);
        }
    }else if($this->input->post('delete')){
        $id_kendaraan    = $this->input->post('id_kendaraan');
        $where = array('id_kendaraan' => $id_kendaraan);
        $sql = $this->BookingModel->DeleteData('kendaraan',$where);
        if($sql){
            echo '<script>alert("Data berhasil dihapus")</script>';
            redirect(base_url('index.php/Kaop_bus/kendaraan'),refresh);
        }else{
            echo '<script>alert("gagal updated")</script>';
            redirect(base_url('index.php/Kaop_bus/kendaraan'),refresh);
        }
    }
}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
       parent::__construct();
       $this->load->model('model', chart);

   }


public function mychart() {

if(!empty($_POST['option'])) {

$val = $_POST['option'];
$result_new=$this->chart->fetch_result($val);

$array = array();
$cols = array();
$rows = array();
$cols[] = array("id"=>"","label"=>" id_kendaraan","pattern"=>"","type"=>"string");
$cols[] = array("id"=>"","label"=>"Count","pattern"=>"","type"=>"number");

foreach ($result_new as $object) {
  $rows[] = array("c"=>array(array("v"=>(string)$object->id_kendaraan),array("v"=>(int)$object->status_count)));
}

$array = array("cols"=>$cols,"rows"=>$rows);
echo json_encode($array);

}
}
}

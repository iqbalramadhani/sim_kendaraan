
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitor_Rutin extends MY_Controller {
	function __construct(){
       parent::__construct();
       $this->load->model('M_sewa');

   }
	//merupakan sebuah kelas yang berisi aksi dari fungsi metode dan dashboard
   public function index(){
    $this->data['sidebar'] = "config/sidebar-misin.php";
    $this->data['MainView'] = "Admin/data_misin.php";
    if(!$this->input->post()){
        //jika tidak ada post
        $this->data['DataMesin'] = $this->M_sewa->GetAllData('monitoring_periodik');
        $this->data['DataKendaraan']= $this->M_sewa->GetAllData('kendaraan');
        $this->load->view('Admin/template', $this->data);
    }else if($this->input->post('add')){
        //tambah data
        $id  = $this->input->post('id');
        $id_kendaraan = $this->input->post('id_kendaraan');
        $oli = $this->input->post('oli');
        $ban      = $this->input->post('ban');
        $air_radiator = $this->input->post('air_radiator');
        $ac = $this->input->post('ac');


        $data = array('id'   => $id,
        'id_kendaraan' => $id_kendaraan,
            'oli' => $oli,
            'ban' => $ban,
            'air_radiator'   => $air_radiator,
            'ac'  => $ac);

        $sql = $this->M_sewa->InsertData('monitoring_periodik',$data);
        if($sql){
            echo'<script>alert("Data Berhasil Ditambahkan")</script>';
            redirect(base_url('index.php/Monitor_Rutin'),refresh);
        }else{
            echo'<script>alert("Data Gagal Ditambahkan")</script>';
            redirect(base_url('index.php/Monitor_Rutin'),refresh);
        }

    }else if($this->input->post('edit')){
        //edit data

        $id  = $this->input->post('id');
          $id_kendaraan = $this->input->post('id_kendaraan');
        $oli = $this->input->post('oli');
        $ban      = $this->input->post('ban');
        $air_radiator = $this->input->post('air_radiator');
        $ac = $this->input->post('ac');


        $data = array('id'   => $id,
        'id_kendaraan' => $id_kendaraan,
            'oli' => $oli,
            'ban' => $ban,
            'air_radiator'   => $air_radiator,
            'ac'  => $ac);


        $where = array('id' => $id);
        $sql = $this->M_sewa->UpdateData('monitoring_periodik',$data, $where);
        if($sql){
            echo '<script>alert("Data berhasil diubah")</script>';
            redirect(base_url('index.php/Monitor_Rutin'),refresh);

        }else{
            echo '<script>alert("Data gagal diubah")</script>';
            redirect(base_url('index.php/Monitor_Rutin'),refresh);
        }
    }else if($this->input->post('delete')){
        $id    = $this->input->post('id');
        $where = array('id' => $id);
        $sql = $this->M_sewa->DeleteData('monitoring_periodik',$where);
        if($sql){
            echo '<script>alert("Data berhasil dihapus")</script>';
            redirect(base_url('index.php/Monitor_Rutin'),refresh);
        }else{
            echo '<script>alert("gagal updated")</script>';
            redirect(base_url('index.php/Monitor_Rutin'),refresh);
        }
    }
}




}
